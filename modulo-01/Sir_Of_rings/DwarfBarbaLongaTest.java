import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {

    private final double DELTA = 1e-1;

    @Test
    public void dwarfDevePerderVida66Percent() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(4);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.sofrerDano();
        assertEquals(100.0, balin.getVida(), DELTA);
    }

    @Test
    public void dwarfNaoDevePerderVida33Percent() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(5);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.sofrerDano();
        assertEquals(110.0, balin.getVida(), DELTA);
    }
}
