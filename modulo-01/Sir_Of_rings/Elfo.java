// C# e C++
//public sealed class Elfo extends Personagem { }
// Java
//public final class Elfo extends Personagem { }
public class Elfo extends Personagem {
    private int indiceFlecha;
    protected int experiencia, qtdExperienciaPorAtaque;
    private static int qtdElfos;

    {
        this.inventario = new Inventario(2);
        this.indiceFlecha = 1;
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;

        this.vida = 100;
    }

    public Elfo(String nome) {
        super(nome);
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }

    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }

    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }

    public int getExperiencia() {
        return this.experiencia;
    }

    private void aumentarXp() {
        //experiencia++;
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }

    public Item getFlecha() {
        return this.inventario.obter(this.indiceFlecha);
    }

    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }

    public String getNome() {
        return super.getNome();
    }

    // DRY - Don't Repeat Yourself
    private boolean podeAtirarFlecha() {
        return this.getFlecha().getQuantidade() > 0;
    }

    public String imprimirResumo() {
        return "Elfo";
    }

    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getFlecha().getQuantidade();
        if (podeAtirarFlecha()) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //experiencia = experiencia + 1;
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
}

