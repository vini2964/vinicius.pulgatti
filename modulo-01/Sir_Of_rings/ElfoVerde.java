import java.util.ArrayList;
import java.util.Arrays;
public class ElfoVerde extends Elfo { 

    private static final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList(
        Arrays.asList(
         "Espada de aço valiriano","Arco de Vidro","Flecha de Vidro"
        ));

    public ElfoVerde(String nome) {
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }

    // public String getNome() {
    // return this.nome +"!"; 
    // }

    // public String getNome() {
    // return super.nome() + "!"; 
    // }

    public void ganharItem(Item item){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida){
            this.inventario.adicionar(item);
        }
    } 
    
    public void perderItem(Item item) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida){
            this.inventario.remover(item);  
    
        }
    } 
}
