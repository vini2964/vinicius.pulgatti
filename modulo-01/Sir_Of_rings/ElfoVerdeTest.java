import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    @Test
    public void elfoVerdeGanha2XpPorUmaFlecha() {
        ElfoVerde celeborn = new ElfoVerde("Celeborn");
        celeborn.atirarFlecha(new Dwarf("Balin"));
        assertEquals(2, celeborn.getExperiencia());
    }

    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida() {
        Elfo celeborn = new ElfoVerde("Celeborn");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celeborn.ganharItem(arcoDeVidro);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }

    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida() {
        Elfo celeborn = new ElfoVerde("Celeborn");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        celeborn.ganharItem(arcoDeMadeira);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Madeira"));
    }

    @Test
    public void elfoVerdePerdeItemComDescricaoValida() {
        Elfo celeborn = new ElfoVerde("Celeborn");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celeborn.perderItem(arcoDeVidro);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
    }

    @Test
    public void elfoVerdeNaoPerdeItemComDescricaoInvalida() {
        Elfo celeborn = new ElfoVerde("Celeborn");
        Item arco = new Item(1, "Arco");
        celeborn.perderItem(arco);
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celeborn.ganharItem(arcoDeVidro);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }
}