
import java.util.*; 
public class ElfosNoturnosPorUltimo implements Estrategia{

    
    protected ArrayList<Elfo> atacantes = new ArrayList<> ();
    
    public ElfosNoturnosPorUltimo(){
    }
    
    public void ElfosVivos(ArrayList <Elfo> atacantes){
        for ( int i=0; i<atacantes.size(); i++){
            if (atacantes.get(i).getStatus() == Status.MORTO) {
                this.atacantes.remove(i);

            }
        }
    }

    public void noturnosPorUltimo(ArrayList <Elfo> atacantes){
        for ( int i=0; i<atacantes.size(); i++){
            for(int j =0; j< atacantes.size()-1; j++){
                Elfo atual = this.atacantes.get(j);
                Elfo proximo = this.atacantes.get(j + 1);
                if (atacantes.get(j + 1).getClass() == ElfoVerde.class) {
                    boolean podeTrocar = atacantes.get(j).getClass() == ElfoNoturno.class;
                    if(podeTrocar){
                        Elfo elfoTrocado = atual;
                        this.atacantes.set(j, proximo);
                        this.atacantes.set(j + 1, elfoTrocado);
                    }
                }
            }
        }
    }   
    
}
