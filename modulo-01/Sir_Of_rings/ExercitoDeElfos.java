import java.util.*;

public class ExercitoDeElfos {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class
            )); 



    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); 

    public void alistar(Elfo elfo) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if (podeAlistar) {
            elfos.add(elfo);
            ArrayList<Elfo> elfosDoStatus = porStatus.get(elfo.getStatus());
            if (elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> buscar(Status status) {
        return this.porStatus.get(status);
    }

    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }

}