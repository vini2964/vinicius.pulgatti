import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class InventarioTest {

    @Test
    public void criarInventarioVazioInformandoQuantidadeInicialDeItens() {
        Inventario inventario = new Inventario(42);
        assertEquals(0, inventario.getItens().size());
    }

    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.getItens().get(0));
    }

    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(3, "Escudo de aço");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }

    @Test
    public void adicionarDoisItensComEspaçoInicialParaUmAdicionaSegundo() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(armadura);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(armadura, inventario.getItens().get(1));
        assertEquals(2, inventario.getItens().size());
    }

    @Test
    public void obterItemNaPrimeiraPosicao() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }

    @Test
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItem() {
        Inventario inventario = new Inventario(10);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void obterItemForaDoLimitComListaCheia() {
        Inventario inventario = new Inventario(10);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertNull(inventario.obter(50));
    }

    @Test
    public void removerItemAntesDeAdicionarProximo() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
        assertEquals(1, inventario.getItens().size());
    }

    @Test
    public void adicionarAposRemover() {
        Inventario inventario = new Inventario(3);
        Item espada = new Item(1, "Espada");
        Item bracelete = new Item(1, "Bracelete de prata");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(bracelete);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(bracelete, inventario.obter(0));
        assertEquals(armadura, inventario.obter(1));
    }

    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Escudo", resultado);
    }

    @Test
    public void getDescricoesItensRemovendoItemNoMeio() {
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item flechas = new Item(3, "Flechas");
        Item botas = new Item(1, "Botas de ferro");

        inventario.adicionar(espada);
        inventario.adicionar(escudo);//
        inventario.adicionar(flechas);
        inventario.adicionar(botas);//
        inventario.adicionar(espada);
        inventario.adicionar(escudo);//
        inventario.adicionar(flechas);
        inventario.adicionar(botas);//

        inventario.remover(1);
        inventario.remover(2);
        inventario.remover(3);
        inventario.remover(4);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Flechas,Espada,Flechas", resultado);
    }

    @Test
    public void getDescricoesItensVazio() {
        Inventario inventario = new Inventario(0);
        String resultado = inventario.getDescricoesItens();
        assertEquals("", resultado);
    }

    @Test
    public void getItemMaiorQuantidadeComVarios() {
        Inventario inventario = new Inventario(3);
        Item lanca = new Item(1, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }

    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
        Inventario inventario = new Inventario(0);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertNull(resultado);
    }

    @Test
    public void getItemMaiorQuantidadeItensComMesmaQuantidade() {
        Inventario inventario = new Inventario(3);
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
    
    @Test
    public void BuscarComInventarioVazio (){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.buscar("Bracelete"));
        
    }
    
    @Test
    public void BuscarComApenasUmItem (){
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1,"termica");
        inventario.adicionar(termica);
        Item resultado = inventario.buscar(new String("termica"));
        assertEquals(termica,resultado); 
        
        
        
    }
    
    @Test
    public void buscarApenasPrimeiroItemComMesmaDescricao() {
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica de 80L de café");
        Item energetico1 = new Item(4, "Litrão de Burn");
        Item energetico2 = new Item(2, "Litrão de Burn");
        inventario.adicionar(termica);
        inventario.adicionar(energetico1);
        inventario.adicionar(energetico2);
        Item resultado = inventario.buscar("Litrão de Burn");
        assertEquals(energetico1, resultado);
    }
    
    @Test
    public void buscarItemForaDoInventarioPreenchido() {
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica de 80L de café");
        Item energetico1 = new Item(4, "Litrão de Burn");
        Item energetico2 = new Item(2, "Litrão de Burn");
        inventario.adicionar(termica);
        inventario.adicionar(energetico1);
        inventario.adicionar(energetico2);
        Item resultado = inventario.buscar("Cházinho de camomila");
        assertNull(resultado);
    }
    
    @Test
    public void InverterArrayListVazio() {
        Inventario inventario = new Inventario(0);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void InverterArrayListUmElemento() {
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica de 80L de café");
        inventario.adicionar(termica);
        inventario.inverter();
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(termica, resultado.get(0));
        assertEquals(1,resultado.size());
        
        
    }
    
    @Test
    public void InverterArrayListDoisElementos() {
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica de 80L de café");
        Item energetico1 = new Item(4, "Litrão de Burn");
        inventario.adicionar(termica);
        inventario.adicionar(energetico1);
        inventario.inverter();
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(energetico1, resultado.get(0));
        assertEquals(termica,resultado.get(1));
        assertEquals(2, resultado.size());
        
    }
    
    @Test
    public void ordenarInventarioVazio() {
        Inventario inventario = new Inventario(0);
        inventario.ordenarItens();
        assertTrue(inventario.getItens().isEmpty());
    }

    @Test
    public void ordenarComApenasUmItem() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarTotalmenteDesordenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarTotalmenteOrdenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarParcialmenteOrdenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarQuantidadesIguais() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(escudo, cafe, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordernarDescComInventarioVazio() {
        Inventario inventario = new Inventario(0);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertTrue(inventario.getItens().isEmpty());
    }

    @Test
    public void ordernarDescComApenasUmItem() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescTotalmenteOrdenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescTotalmenteDesordenado() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item cafe = new Item(3, "Térmica de café");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescParcialmenteDesordenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }    

    @Test
    public void ordernarDescQuantidadesIguais() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(escudo, cafe, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }    
}