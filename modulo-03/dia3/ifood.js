function cardapioTiaIFood( veggie = true, comLactose = true ) {
  var cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  console.log(cardapio)
  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }

  cardapio.push ('pastel de carne','empada de legumes marabijosa')

   if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)

    cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ),1 )
    cardapio.splice( cardapio.indexOf( 'pastel de carne' ),1 )
  }
  return cardapioTiaIFood
}

cardapioTiaIFood() // esperado: [ 'cuca de uva', 'pastel de queijo', 'empada de legumes marabijosa' ]
