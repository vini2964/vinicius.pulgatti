/* var luke = {
  nome :"Luke",
  atacarComSabre: function() {
    console.log ({this.nome})
  }
}
luke.atacarComSabre()

var Mace = {
  nome :"Mace",
  atacarComSabre: function() {  
    console.log ({this.nome})
  }

} */

/* function Jedi(nome) {
  this.nome = nome
   
}
  Jedi.prototype.atacarComSber = function() {
    console.log( `${ this.nome } atacar com sabre!` )
  } */


  class Jedi {
    constructor(nome) {
      this.nome = nome; 
    }

    atacarComSabre(){
      console.log( `${ this.nome } atacar com sabre!` )

    }
  }



var luke = new Jedi('Luke')
luke.atacarComSabre()
var maceWindu = new Jedi('Mace Windu')
maceWindu.atacarComSabre()


//String.prototype.shimeki = function() {
//  return this + "Shimeji!!!"
//} adiciona o metodo shimeki no objeto string, desta forma todas as strings poderao acessar este metodo por "string".shimeki.


var funcShimeji = function(uppercase = false) {
  var texto = this + " Shimeji!!!"
  return uppercase ? texto.toUpperCase() : texto
}

String.prototype.shimeji = funcShimeji
Boolean.prototype.shimeji = funcShimeji
console.log("melhor cogumelo é:".shimeji(true))
console.log("melhor cogumelo é:".shimeji(false))
var flag = true
console.log(flag.shimeji()) // true Shimeji!!!
