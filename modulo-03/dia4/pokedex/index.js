pokemonAnt = 0

function Sorte() {
  var nPoke = Math.floor(Math.random() * 802 + 1)
  capturar(nPoke)
}

function capturaNumId() {
  numId = document.getElementById('nPoke').value;
  capturar(numId)
};

 
function capturar(nPoke){
 
  if( nPoke <  802 ){
    if (pokemonAnt !== nPoke) {
      var pokeApi = new PokeApi()
      pokeApi.buscar(nPoke)
          .then( pokemonServidor => {
            var poke = new Pokemon( pokemonServidor )
      pokeApi.buscar(nPoke)
      pokemonAnterior = nPoke
            renderizaPokemonNaTela( poke )
        } )
        pokemonAnt = nPoke
    }  
    
      } else {
     alert("Favor insira um ID valido") 
      }  
  }



function renderizaPokemonNaTela( pokemon ) {
  var dadosPokemon = document.getElementById('dadosPokemon')
  
  var nome = dadosPokemon.querySelector('.nome')
  nome.innerText =  `Pokemon: ${pokemon.nome}`
  
  var imgPokemon = dadosPokemon.querySelector('.thumb')
  imgPokemon.src = pokemon.thumbUrl
  
  var id = dadosPokemon.querySelector('.id')
  id.innerText = pokemon._id
 
  var altura = dadosPokemon.querySelector('.altura')
  altura.innerText = `Altura: ${pokemon._altura} cm`
  
  var peso = dadosPokemon.querySelector('.peso')
  peso.innerText = `Peso: ${pokemon._peso} kg` 
  
  var tipoPok = dadosPokemon.querySelector('.tipokemon')
  tipoPok.innerText = `Tipo: `
  for (let i = 0; i < 2; i++) {
    var tipoPokemon = dadosPokemon.querySelector(`.tipo${i}`)
    let nenhumTipo = pokemon.tipo[i] === undefined
    if(nenhumTipo) {
      tipoPokemon.innerText = ''
    } else {
    tipoPokemon.innerText = pokemon.tipo[i]
    }
  }
  var velocidade = dadosPokemon.querySelector('.velocidade')
  velocidade.innerText = `velocidade: ${pokemon.stats[0]}%`
  var defesaEspecial = dadosPokemon.querySelector('.defesaEspec')
  defesaEspecial.innerText = `Defesa Especial: ${pokemon.stats[1]}%`
  var AtaqueEsp = dadosPokemon.querySelector('.especiAta')
  AtaqueEsp.innerText = `Ataque Especial: ${pokemon.stats[2]}%`
  var Defense = dadosPokemon.querySelector('.defesa')
  Defense.innerText = `Defesa: ${pokemon.stats[3]}%`
  var Ataque = dadosPokemon.querySelector('.ataque')
  Ataque.innerText = `Ataque: ${pokemon.stats[4]}%`
  var HP = dadosPokemon.querySelector('.hp')
  HP.innerText = `Hp: ${pokemon.stats[5]}`

}
