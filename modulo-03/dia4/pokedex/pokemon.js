class Pokemon {
  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height * 10
    this._peso = objVindoDaApi.weight / 10
    this._id = objVindoDaApi.id
    this._tipo = objVindoDaApi.types.map( t => t.type.name )
    this._stats = objVindoDaApi.stats.map( s => s.base_stat)
  }


  get id() {
    return this._id
  }

  get tipo() {
    return this._tipo
  }

  get stats() {
    return this._stats
  }
}
