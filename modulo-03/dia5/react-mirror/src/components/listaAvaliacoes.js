import React from 'react'

import {Link} from 'react-router-dom';
function ordenacaoTemp(a, b) {
  return a.temporada - b.temporada
}

function ordenacaoEpi(a, b) {
return a.ordemEpisodio - b.ordemEpisodio
}

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  const listaOrdenada = listaEpisodios.avaliados.sort( ordenacaoTemp,ordenacaoEpi )
      
      return listaOrdenada.map( e => <li key={ e.id }> <Link to={{pathname : `/episodio/${ e.id }`,state :{ e }}}>  { `${ e.nome} - ${ e.nota }` }</Link></li>)

}


export default ListaAvaliacoes