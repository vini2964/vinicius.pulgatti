import React, { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi';
import './telaDetalheEpisodio.css'


 export default class TelaDetalheEpisodio extends Component {
    constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
        detalhes: {},
    }
}

    componentDidMount() {
        this.episodiosApi.buscarDetalhes(this.props.location.state.e.id).then(detalhesDoServidor => {
                this.setState({
                    detalhes: detalhesDoServidor.data[0]
                })
            })
     }
    
      
    render(){
        const { e } = this.props.location.state
        const { detalhes } = this.state
    
        return (
        <React.Fragment>     
            <div className="posicao">
                <h3>{ e.nome }</h3>
                <div><img src={ e.thumbUrl }/></div>
                
                <div>{ e.temporadaEpisodio }</div>
                <div>{ e.duracaoEmMin }</div>
                <div>{detalhes.sinopse}</div>
                <div>{detalhes.dataEstreia = (new Date(detalhes.dataEstreia).toLocaleDateString('pt')) }</div>
                <div>{ e.nota}</div>
                <div>{detalhes.notaImdb / 2}</div>
            </div >

        </React.Fragment>
    )
    }

}

    