import React from 'react'
import { Link } from 'react-router-dom'
import MeuBotao from '../components/shared/meuBotao'
import './shared/meuBotao.css'
/* 
function OrdenarDeAaZ(){
  const OrdenadaDeAaZ = a.nome - b.nome
}

function OrdenarDeZ(){
  const OrdenadaDeAaZ = a.nome - b.nome
} */


 const TodosEpisodios = props => {
  const { listaEpisodios } = props.location.state
  const temporadaOrdenada = listaEpisodios.todos.sort()
  return (
    <React.Fragment>
      { temporadaOrdenada.map( e => <li key={ e.id }><Link className="Link" to={{ pathname: `/episodio/${ e.id }`, state: { e } }} value={ e.id }>{ `${ e.nome }  - ${ e.temporada } - ${ e.duracao } minutos` }</Link></li> ) }
      <div className="botoes">
        <MeuBotao cor="verde" texto="botão 1" />
        <MeuBotao cor="azul" texto="botão 2" />
      </div>  
    </React.Fragment>
    )
}

export default TodosEpisodios