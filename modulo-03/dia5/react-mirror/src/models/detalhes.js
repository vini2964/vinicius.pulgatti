import '../utils/number-prototypes'
import detalhesApi from './detalhesApi'

export default class Detalhes {
  constructor( id, notaImdb, sinopse,dataEstreia) {
    this.id = id
    this.notaImdb = notaImdb
    this.sinopse = sinopse
    this.dataEstreia = dataEstreia
    this.detalhesApi = new detalhesApi()
  }

}