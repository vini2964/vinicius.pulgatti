package br.com.dbccompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AulaSpring2Application {

	public static void main(String[] args) {
		SpringApplication.run(AulaSpring2Application.class, args);
	}

}
