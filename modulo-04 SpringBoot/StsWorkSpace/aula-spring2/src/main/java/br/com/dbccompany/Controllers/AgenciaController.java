package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.AgenciaService;
import br.com.dbccompany.entity.Agencia;

@Controller
@RequestMapping("/api/agencia")
public class AgenciaController {
	
	
	
	@Autowired
	AgenciaService agenciaService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Agencia> listaAgencias(){
		return agenciaService.allAgencias();
		
		}
	
	@PostMapping(value="/nova")
	@ResponseBody
	public Agencia novaAgencia(@RequestBody Agencia agencia) {
		return agenciaService.save(agencia);
		
	}
	
	@PutMapping(value="/editarAgencia/{id}")
	@ResponseBody
	public Agencia alterarBancoPorId(@PathVariable long id, Agencia agencia) {
		return agenciaService.editarAgencia(id, agencia);
				
	}
}
