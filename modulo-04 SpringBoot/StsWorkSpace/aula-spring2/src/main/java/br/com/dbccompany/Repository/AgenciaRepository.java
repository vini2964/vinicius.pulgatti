package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Agencia;
import br.com.dbccompany.entity.Banco;

public interface AgenciaRepository extends CrudRepository<Agencia, Long>  {
	
	Agencia findByCodigo(long codigo);	
	List<Agencia> findByBanco(Banco banco);


}
