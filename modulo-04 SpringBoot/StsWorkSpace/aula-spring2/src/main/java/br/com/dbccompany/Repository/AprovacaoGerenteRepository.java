package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.AprovacaoGerente;
import br.com.dbccompany.entity.EmprestimoAprovado;
import br.com.dbccompany.entity.SolicitacaoEmprestimo;
import br.com.dbccompany.entity.Usuario;





public interface AprovacaoGerenteRepository extends CrudRepository<AprovacaoGerente, Long>  {
	
	AprovacaoGerente findByUsuario(List<Usuario> usuario);

}
