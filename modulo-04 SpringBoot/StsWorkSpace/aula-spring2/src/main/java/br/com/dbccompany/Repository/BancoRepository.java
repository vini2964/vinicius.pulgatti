package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Agencia;
import br.com.dbccompany.entity.Banco;





public interface BancoRepository extends CrudRepository<Banco, Long>  {
	
	Banco findByCodigo(long codigo);
	Banco findByNome(String nome);
	Banco findByagencia(List<Agencia> agencia);

}
