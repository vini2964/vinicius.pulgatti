package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Agencia;
import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Conta;
import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.entity.Usuario;


public interface ClienteRepository extends CrudRepository<Cliente, Long>  {
	
	Cliente findByNome(String nome);
	Cliente findByCpf(long cpf);
	Cliente findByRg(long rg);
	
	Cliente findByagencia(List<Agencia> agencia);

}
