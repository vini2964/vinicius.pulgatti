package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Conta;
import br.com.dbccompany.entity.ContaType;
import br.com.dbccompany.entity.Movimentacao;





public interface ContaRepository extends CrudRepository<Conta, Long>  {
	
	Conta findByNumero(long numero);
	Conta findBysaldo(long saldo);
	List<Conta> findBytipo(ContaType tipo);
	Conta findBycliente(List<Cliente> cliente);
	Conta findBymovimentacao(List<Movimentacao> movimentacao);
	
}
