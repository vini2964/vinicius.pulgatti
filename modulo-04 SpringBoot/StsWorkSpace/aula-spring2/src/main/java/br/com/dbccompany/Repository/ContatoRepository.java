package br.com.dbccompany.Repository;



import java.util.List;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.entity.Contato;
import br.com.dbccompany.entity.TipoContato;





public interface ContatoRepository extends CrudRepository<Contato, Long>  {
	
	Contato findByValor(String valor);
	Contato findBytipocontato(List<TipoContato> tipoContato);

}
