package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.entity.EmprestimoAprovado;
import br.com.dbccompany.entity.StatusEmprestimo;





public interface EmprestimoAprovadoRepository extends CrudRepository<EmprestimoAprovado, Long>  {
	
	EmprestimoAprovado findByValorAprovado(long valorAprovado);
	
	EmprestimoAprovado findByStatus(StatusEmprestimo status);


}
