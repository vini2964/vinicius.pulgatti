package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Integer>  {
	
	Endereco findByLogradouro(String Logradouro);
	Endereco findByNumero(Integer numero);
	Endereco findByComplemento(String complemento);
	Endereco findByBairro(String bairro);
	Endereco findBycliente(List<Cliente> cliente);
	
}
