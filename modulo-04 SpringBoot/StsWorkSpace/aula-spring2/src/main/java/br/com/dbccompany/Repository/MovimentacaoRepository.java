package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Conta;
import br.com.dbccompany.entity.Movimentacao;
import br.com.dbccompany.entity.MovimentacaoType;





public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Long>  {
	
	
	Movimentacao findByconta(List<Conta> conta);

}
