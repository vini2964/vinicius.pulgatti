package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.Conta;
import br.com.dbccompany.entity.ContaType;
import br.com.dbccompany.entity.Movimentacao;
import br.com.dbccompany.entity.SolicitacaoEmprestimo;

public interface SolicitacaoEmprestimoRepository extends CrudRepository<SolicitacaoEmprestimo, Long>  {
	
	SolicitacaoEmprestimo findByvalorSolic(long valorSolic);
	
	SolicitacaoEmprestimo findByconta(List<Conta> conta);
	
}
