package br.com.dbccompany.Repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.entity.Cliente;
import br.com.dbccompany.entity.TipoUsuario;
import br.com.dbccompany.entity.Usuario;





public interface UsuarioRepository extends CrudRepository<Usuario, Long>  {
	
	
	Usuario findBytipouser(List<TipoUsuario> tipouser);
	Usuario findByCliente(Cliente cliente);


}
