package br.com.dbccompany.Services;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.AgenciaRepository;
import br.com.dbccompany.entity.Agencia;



@Service
public class AgenciaService {
	
	@Autowired
	public AgenciaRepository agenciaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia save(Agencia agencia) {
		
		return agenciaRepository.save(agencia);
		
	} 
	
	public List<Agencia> allAgencias() {
		return (List<Agencia>) agenciaRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia editarAgencia(long id, Agencia agencia) {
		agencia.setId(id);
		return agenciaRepository.save(agencia);
	}
	

	
}
