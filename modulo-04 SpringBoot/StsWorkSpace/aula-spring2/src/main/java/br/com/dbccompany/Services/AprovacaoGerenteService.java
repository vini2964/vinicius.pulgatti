package br.com.dbccompany.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.AprovacaoGerenteRepository;
import br.com.dbccompany.entity.AprovacaoGerente;




public class AprovacaoGerenteService {
	
	@Autowired
	private AprovacaoGerenteRepository aprovacaoGerenteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private AprovacaoGerenteRepository save(AprovacaoGerente aprovacao) {
		
		return (AprovacaoGerenteRepository) aprovacaoGerenteRepository.save(aprovacao);
	}
	
	@SuppressWarnings("unused")
	private Optional<AprovacaoGerente> buscarBanco(long id) {
		return aprovacaoGerenteRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private AprovacaoGerente editarBanco(long id, AprovacaoGerente aprovacao) {
		aprovacao.setId(id);
		return aprovacaoGerenteRepository.save(aprovacao);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void removerAprovacao(AprovacaoGerente aprovacao) {
		aprovacaoGerenteRepository.delete(aprovacao);
		
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void removerAprovacaoId(long id, AprovacaoGerente aprovacao) {
		aprovacao.setId(id);
		aprovacaoGerenteRepository.deleteById(id);
		
	}
}
