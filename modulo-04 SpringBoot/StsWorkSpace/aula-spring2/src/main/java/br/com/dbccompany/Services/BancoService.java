package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.BancoRepository;
import br.com.dbccompany.entity.Banco;

@Service
public class BancoService {

	@Autowired
	public BancoRepository bancoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Banco save(Banco banco) {
		
		return bancoRepository.save(banco);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Banco> buscarBanco(long id) {
		return bancoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco editarBanco(long id, Banco banco) {
		banco.setId(id);
		return bancoRepository.save(banco);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerBanco( Banco banco)  {
		bancoRepository.delete(banco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void removerBancoId(long id, Banco banco) {
		banco.setId(id);
		bancoRepository.deleteById(id);
	}
	public List<Banco> allBancos() {
		return (List<Banco>) bancoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco buscarPorCodigo(long codigo) {
		return bancoRepository.findByCodigo(codigo);
		
		
	}
	
	
	
	
}
