package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")

public class Agencia {

	@Id
	@GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "CODIGO")
	private long codigo;
	
	@ManyToOne
    @JoinColumn(name = "id_banco")
	private Banco banco;
	
	@OneToOne(mappedBy = "agencia")
	private Endereco endereco;
	
	 @ManyToMany
	 @JoinTable(name = "AGENCIA_CLIENTE",
	            joinColumns = {
	                @JoinColumn(name = "ID_AGENCIA")},
	            inverseJoinColumns = {
	                @JoinColumn(name = "ID_CLIENTE")})
	    private List<Cliente> cliente = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}
	 
	 
}





