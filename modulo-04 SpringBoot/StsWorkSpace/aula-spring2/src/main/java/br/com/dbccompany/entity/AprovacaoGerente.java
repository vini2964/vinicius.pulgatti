package br.com.dbccompany.entity;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "APROVACAO_GERENTE_SEQ", sequenceName = "APROVACAO_GERENTE_SEQ")
public class AprovacaoGerente {
	
	@Id
	@GeneratedValue(generator = "APROVACAO_GERENTE_SEQ", strategy = GenerationType.SEQUENCE)	
	private long id;
	private boolean validaUsuario;
	
	
	@OneToMany(mappedBy = "aprovacao")
    private List<Usuario> usuario = new ArrayList<>();
	
	@OneToOne()
	private SolicitacaoEmprestimo solicitacao;
	
	@OneToOne()
	private EmprestimoAprovado aprovacao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Usuario> getUsuario() {
		return usuario;
	}

	public void setUsuario(List<Usuario> usuario) {
		this.usuario = usuario;
	}

	public SolicitacaoEmprestimo getSolicitacao() {
		return solicitacao;
	}

	public void setSolicitacao(SolicitacaoEmprestimo solicitacao) {
		this.solicitacao = solicitacao;
	}

	public EmprestimoAprovado getAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(EmprestimoAprovado aprovacao) {
		this.aprovacao = aprovacao;
	}
	
	public boolean isValidaUsuario() {
		return validaUsuario;
	}
	
	public void setValidaUsuario(boolean validaUsuario) {
		this.validaUsuario = validaUsuario;
	}

	
	
	
}
