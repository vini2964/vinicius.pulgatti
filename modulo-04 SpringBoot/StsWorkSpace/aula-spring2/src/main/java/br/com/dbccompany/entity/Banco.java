package br.com.dbccompany.entity;




import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
public class Banco {

	@Id
	@GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;

	@Column(name = "CODIGO")
	private long codigo;

	@Column(name = "NOME")
	private String nome;
	
	@OneToMany(mappedBy = "banco")
    private List<Agencia> agencia = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencia() {
		return agencia;
	}

	public void setAgencia(List<Agencia> agencia) {
		this.agencia = agencia;
	}
	
	
}
