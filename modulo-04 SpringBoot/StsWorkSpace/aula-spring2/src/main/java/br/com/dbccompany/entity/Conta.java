package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;


@Entity
@SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")

public class Conta {
	 	@Id
	 	@GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
	    private long id;
	 	private long saldo;
	 	private long numero;
	 	
	 	@Enumerated(EnumType.STRING)
	 	@JoinColumn(name = "tipo")
	    private ContaType tipo;
	 	
	 	@ManyToMany
		@JoinTable(name = "CONTA_CLIENTE",
		            joinColumns = {
		                @JoinColumn(name = "ID_CONTA")},
		            inverseJoinColumns = {
		                @JoinColumn(name = "ID_CLIENTE")})
	 	private List<Cliente> cliente = new ArrayList<>();
	 	
	 	@ManyToMany
		@JoinTable(name = "CONTA_MOVIMENTACAO",
		            joinColumns = {
		                @JoinColumn(name = "ID_CONTA")},
		            inverseJoinColumns = {
		                @JoinColumn(name = "ID_MOVIMENTACAO")})
	 	private List<Movimentacao> movimentacao = new ArrayList<>();
	 	
	 	@ManyToOne
	    @JoinColumn(name = "id_solicitacaoemprestimo")
		private SolicitacaoEmprestimo solicitacaoEmp;
	 	
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getSaldo() {
			return saldo;
		}

		public void setSaldo(Long saldo) {
			this.saldo = saldo;
		}

		public long getNumero() {
			return numero;
		}

		public void setNumero(long numero) {
			this.numero = numero;
		}

		public ContaType getTipo() {
			return tipo;
		}

		public void setTipo(ContaType tipo) {
			this.tipo = tipo;
		}

		public List<Cliente> getCliente() {
			return cliente;
		}

		public void pushCliente(Cliente... cliente) {
			this.cliente.addAll(Arrays.asList(cliente));
		}

		public List<Movimentacao> getMovimentacao() {
			return movimentacao;
		}

		public void pushMovimentacao(Movimentacao...movimentacao) {
			this.movimentacao.addAll(Arrays.asList(movimentacao));
		}

		public SolicitacaoEmprestimo getSolicitacaoEmp() {
			return solicitacaoEmp;
		}

		
	 	
	 	
}
