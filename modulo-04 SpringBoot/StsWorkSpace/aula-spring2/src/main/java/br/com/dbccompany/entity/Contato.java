package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")

public class Contato {
	 	@Id
	    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
	    private Integer id;
	 	private String valor;
	 	
	 	@OneToMany(mappedBy = "contato")
	    private List<TipoContato> tipocontato = new ArrayList<>();

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getValor() {
			return valor;
		}

		public void setValor(String valor) {
			this.valor = valor;
		}

		public List<TipoContato> getTipocontato() {
			return tipocontato;
		}

		public void setTipocontato(List<TipoContato> tipocontato) {
			this.tipocontato = tipocontato;
		}
	 	
}
