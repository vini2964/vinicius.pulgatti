package br.com.dbccompany.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "EMPRESTIMO_APROVADO_SEQ", sequenceName = "EMPRESTIMO_APROVADO_SEQ")

public class EmprestimoAprovado {
	@Id
	@GeneratedValue(generator = "EMPRESTIMO_APROVADO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	private long valorAprovado;
	
	@OneToOne(mappedBy = "aprovacao")
	private AprovacaoGerente aprovado;
	
	@OneToOne()
	private StatusEmprestimo status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getValorAprovado() {
		return valorAprovado;
	}

	public void setValorAprovado(Long valorAprovado) {
		this.valorAprovado = valorAprovado;
	}

	public AprovacaoGerente getAprovado() {
		return aprovado;
	}

	public void setAprovado(AprovacaoGerente aprovado) {
		this.aprovado = aprovado;
	}
	
}
