package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(allocationSize = 1, name = "	ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
public class Endereco {

    @Id
	@GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String logradouro;
    private Integer numero;
    private String complemento;
    private String bairro;
    private String cidade;

    @OneToOne
    @JoinColumn(name = "id_agencia")
    private Agencia agencia;
    
    @ManyToMany
	@JoinTable(name = "cliente_endereco",
            joinColumns = {
            		@JoinColumn(name = "ID_ENDERECO")},
	            	inverseJoinColumns = {
	                @JoinColumn(name = "ID_CLIENTE")})
	    private List<Cliente> cliente = new ArrayList<>();
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void pushCliente(Cliente... cliente) {
		this.cliente.addAll(Arrays.asList(cliente));
	}

    
}
