package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "	MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")
public class Movimentacao {
	
	@Id
	@GeneratedValue(generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Enumerated(EnumType.STRING)
    private MovimentacaoType Movtipo;
	
	@ManyToMany(mappedBy = "movimentacao")
	private List<Conta> conta = new ArrayList<>();
	
	public List<Conta> getConta() {
		return conta;
	}

	public void setConta(List<Conta> conta) {
		this.conta = conta;
	}

	@ManyToMany(mappedBy = "mov")
	private List<StatusEmprestimo> status = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MovimentacaoType getMovtipo() {
		return Movtipo;
	}

	public void setMovtipo(MovimentacaoType movtipo) {
		Movtipo = movtipo;
	}



	public List<StatusEmprestimo> getStatus() {
		return status;
	}

	public void setStatus(List<StatusEmprestimo> status) {
		this.status = status;
	}
	
	
	
}
