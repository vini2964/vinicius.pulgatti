package br.com.dbccompany.entity;

public enum MovimentacaoType {
	Deposito, Saque, Transferencia, Pagamento;
}
