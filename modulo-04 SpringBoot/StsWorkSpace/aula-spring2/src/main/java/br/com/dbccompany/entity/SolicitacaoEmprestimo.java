package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "	SOLICITACAO_EMPRESTIMO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")
public class SolicitacaoEmprestimo {
	@Id
	@GeneratedValue(generator = "SOLICITACAO_EMPRESTIMO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	private long valorSolic;
	
	
	@OneToMany(mappedBy = "solicitacaoEmp")
    private List<Conta> conta = new ArrayList<>();
	
	@OneToOne(mappedBy = "solicitacao")
	private AprovacaoGerente aprovacao;
	
	

	public AprovacaoGerente getAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(AprovacaoGerente aprovacao) {
		this.aprovacao = aprovacao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Conta> getConta() {
		return conta;
	}

	public void setConta(List<Conta> conta) {
		this.conta = conta;
	}

	public Long getValorSolic() {
		return valorSolic;
	}

	public void setValorSolic(Long valorSolic) {
		this.valorSolic = valorSolic;
	}
	
	
	
}
