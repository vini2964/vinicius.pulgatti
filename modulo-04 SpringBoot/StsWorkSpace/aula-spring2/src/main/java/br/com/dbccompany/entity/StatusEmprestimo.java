package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;

@Entity
@SequenceGenerator(allocationSize = 1, name = "	STATUS_EMPRESTIMO_EMPRESTIMO_SEQ", sequenceName = "STATUS_EMPRESTIMO_EMPRESTIMO_SEQ")
public class StatusEmprestimo {
	@Id
	@GeneratedValue(generator = "STATUS_EMPRESTIMO_EMPRESTIMO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	private long parcelamento;
	
	@OneToOne(mappedBy = "status")
	private EmprestimoAprovado aprovado;
	
	@ManyToMany
	@JoinTable(name = "emprestimo_movimentacao",
            joinColumns = {
            		@JoinColumn(name = "ID_status_emprestimos")},
	            	inverseJoinColumns = {
	                @JoinColumn(name = "ID_movimentacao")})
	    private List<Movimentacao> mov = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getParcelamento() {
		return parcelamento;
	}

	public void setParcelamento(long parcelamento) {
		this.parcelamento = parcelamento;
	}

	public EmprestimoAprovado getAprovado() {
		return aprovado;
	}

	public void setAprovado(EmprestimoAprovado aprovado) {
		this.aprovado = aprovado;
	}

	public List<Movimentacao> getMov() {
		return mov;
	}

	public void pushMov(Movimentacao... mov) {
		this.mov.addAll(Arrays.asList(mov));
	}
	
	
}
