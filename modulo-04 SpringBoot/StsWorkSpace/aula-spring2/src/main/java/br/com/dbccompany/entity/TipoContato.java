package br.com.dbccompany.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "	TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")

public class TipoContato {

	 	@Id
		@GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
	    private Integer id;
	 	private String nome;
	 	private Long quantidade;
	 	
	 	@ManyToOne
	    @JoinColumn(name = "id_contato")
		private Contato contato;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public Long getQuantidade() {
			return quantidade;
		}

		public void setQuantidade(Long quantidade) {
			this.quantidade = quantidade;
		}

		public Contato getContato() {
			return contato;
		}

		public void setContato(Contato contato) {
			this.contato = contato;
		}
	 	
	 	
}
