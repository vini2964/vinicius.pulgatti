package br.com.dbccompany.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
public class Usuario {
	
	@Id
	@GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@ManyToOne
    @JoinColumn(name = "id_aprovacao_gerente")
	private AprovacaoGerente aprovacao;
	
	@OneToOne(mappedBy = "usuarios")
	private Cliente cliente;
	
	@OneToOne(mappedBy = "usuario")
	private TipoUsuario tipouser;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AprovacaoGerente getAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(AprovacaoGerente aprovacao) {
		this.aprovacao = aprovacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public TipoUsuario getTipouser() {
		return tipouser;
	}

	public void setTipouser(TipoUsuario tipouser) {
		this.tipouser = tipouser;
	}
	
	
}
