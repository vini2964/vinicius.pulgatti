package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Service.AcessosService;

@Controller
@RequestMapping("/api/Acessos")
public class AcessosController {
	
	@Autowired
	AcessosService acessosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Acessos> listaAcessos(){
		return acessosService.allAcessos();
			
	}
	
	@PostMapping(value="/novoAcesso")
	@ResponseBody
	public Acessos salvaAcessos(@RequestBody Acessos acesso) {
		
		return acessosService.save(acesso);

	}
	
	@PutMapping(value="/editarAcesso/{id}")
	@ResponseBody
	public Acessos alterarAcessosPorID(@PathVariable long id, @RequestBody Acessos acesso) {
		return acessosService.editarAcessos(id, acesso);
				
	}
}
