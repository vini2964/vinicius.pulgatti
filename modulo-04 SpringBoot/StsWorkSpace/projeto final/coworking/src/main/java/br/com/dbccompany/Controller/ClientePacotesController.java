package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.ClientePacotes;
import br.com.dbccompany.Service.ClientePacotesService;

@Controller
@RequestMapping("/api/ClientePacotes")
public class ClientePacotesController {

	@Autowired
	ClientePacotesService clientePacotesService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<ClientePacotes> listaClientePacotes(){
		return clientePacotesService.allClientePacotes();
			
	}
	
	@PostMapping(value="/novoClientePacotes")
	@ResponseBody
	public ClientePacotes salvaClientePacotes(@RequestBody ClientePacotes clientePacotes) {
		
		return clientePacotesService.save(clientePacotes);
		
	}
	
	@PutMapping(value="/editarClientePacotes/{id}")
	@ResponseBody
	public ClientePacotes alterarClientePacotesPorID(@PathVariable long id, @RequestBody ClientePacotes clientePacotes) {
		return clientePacotesService.editarClientePacotes(id, clientePacotes);
				
	}
}
