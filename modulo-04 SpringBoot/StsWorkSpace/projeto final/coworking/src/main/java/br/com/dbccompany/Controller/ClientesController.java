package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Service.ClientesService;

@Controller
@RequestMapping("/api/Clientes")
public class ClientesController {
	
	@Autowired
	ClientesService clientesService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Clientes> listaClientes(){
		return clientesService.allClientes();
			
	}
	
	@PostMapping(value="/novoCliente")
	@ResponseBody
	public Clientes salvaClientes(@RequestBody Clientes clientes) {
		
		return clientesService.save(clientes);
		//TODO Na criação deve por regra ser obrigatório ser criado um contato do tipo: Email e um do tipo: telefone

	}
	
	@PutMapping(value="/editarCliente/{id}")
	@ResponseBody
	public Clientes alterarClientesPorID(@PathVariable long id, @RequestBody Clientes clientes) {
		return clientesService.editarClientes(id, clientes);
				
	}
}
