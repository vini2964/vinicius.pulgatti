package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Service.ContatoService;

@Controller
@RequestMapping("/api/Contato")
public class ContatoController {
	
	@Autowired
	ContatoService ContatoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Contato> listaContato(){
		return ContatoService.allContato();
			
	}
	
	@PostMapping(value="/novoContato")
	@ResponseBody
	public Contato salvaContato(@RequestBody Contato contato) {
		return ContatoService.save(contato);
		
	}
	
	@PutMapping(value="/editarContato/{id}")
	@ResponseBody
	public Contato alterarContatoPorID(@PathVariable long id, @RequestBody Contato contato) {
		return ContatoService.editarContato(id, contato);
				
	}

}
