package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Service.ContratacaoService;

@Controller
@RequestMapping("/api/Contratacao")
public class ContratacaoController {
	
	@Autowired
	ContratacaoService contratacaoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Contratacao> listaContratacao(){
		return contratacaoService.allContratacao();
			
	}
	
	@PostMapping(value="/novoContratacao")
	@ResponseBody
	public double salvaContratacao(@RequestBody Contratacao contratacao) {
		return contratacaoService.save(contratacao);
		
	}
	
	@PutMapping(value="/editarContratacao/{id}")
	@ResponseBody
	public Contratacao alterarContratacaoPorID(@PathVariable long id, @RequestBody Contratacao contratacao) {
		return contratacaoService.editarContratacao(id, contratacao);
				
	}
}
