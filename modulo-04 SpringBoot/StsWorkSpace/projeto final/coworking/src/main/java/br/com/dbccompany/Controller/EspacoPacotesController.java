package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.EspacoPacotes;
import br.com.dbccompany.Service.EspacoPacotesService;

@Controller
@RequestMapping("/api/espacoPacotes")
public class EspacoPacotesController {
	
	@Autowired
	EspacoPacotesService espacoPacotesService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<EspacoPacotes> listaEspacoPacotes(){
		return espacoPacotesService.allEspacoPacotes();
			
	}
	
	@PostMapping(value="/novoEspacoPacotes")
	@ResponseBody
	public EspacoPacotes salvaEspacoPacotes(@RequestBody EspacoPacotes espacoPacotes) {
		return espacoPacotesService.save(espacoPacotes);
		
	}
	
	@PutMapping(value="/editarEspacoPacotes/{id}")
	@ResponseBody
	public EspacoPacotes alterarEspacoPacotesPorID(@PathVariable long id, @RequestBody EspacoPacotes espacoPacotes) {
		return espacoPacotesService.editarEspacoPacotes(id, espacoPacotes);
				
	}
}
