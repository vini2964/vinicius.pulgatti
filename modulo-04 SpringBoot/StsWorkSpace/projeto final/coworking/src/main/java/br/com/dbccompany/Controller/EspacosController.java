package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Service.EspacosService;

@Controller
@RequestMapping("/api/Espacos")
public class EspacosController {
	
	@Autowired
	EspacosService EspacosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Espacos> listaEspacos(){
		return EspacosService.allEspacos();
			
	}
	
	@PostMapping(value="/novoEspaco")
	@ResponseBody
	public Espacos salvaEspacos(@RequestBody Espacos espacos) {
		return EspacosService.save(espacos);
		
	}
	
	@PutMapping(value="/editarUsuario/{id}")
	@ResponseBody
	public Espacos alterarEspacosPorID(@PathVariable long id, @RequestBody Espacos espacos) {
		return EspacosService.editarEspacos(id, espacos);
				
	}

}
