package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Service.PacotesService;

@Controller
@RequestMapping("/api/Pacotes")
public class PacotesController {
	
	@Autowired
	PacotesService pacotesService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Pacotes> listaPacotes(){
		return pacotesService.allPacotes();
			
	}
	
	@PostMapping(value="/novoPacotes")
	@ResponseBody
	public Pacotes salvaPacotes(@RequestBody Pacotes pacotes) {
		return pacotesService.save(pacotes);
		
	}
	
	@PutMapping(value="/editarPacotes/{id}")
	@ResponseBody
	public Pacotes alterarPacotesPorID(@PathVariable long id, @RequestBody Pacotes pacotes) {
		return pacotesService.editarPacotes(id, pacotes);
				
	}
}
