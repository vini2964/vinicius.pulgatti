package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Service.PagamentosService;

@Controller
@RequestMapping("/api/Pagamentos")

public class PagamentosController {

	@Autowired
	PagamentosService pagamentosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Pagamentos> listaPagamentos(){
		return pagamentosService.allPagamentos();
			
	}
	
	@PostMapping(value="/novoPagamentos")
	@ResponseBody
	public Pagamentos salvaPagamentos(@RequestBody Pagamentos pagamentos) {
		
		return pagamentosService.save(pagamentos);

	}
	
	@PutMapping(value="/editarPagamentos/{id}")
	@ResponseBody
	public Pagamentos alterarPagamentosPorID(@PathVariable long id, @RequestBody Pagamentos pagamento) {
		return pagamentosService.editarPagamentos(id, pagamento);
				
	}
}
