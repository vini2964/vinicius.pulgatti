package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoId;
import br.com.dbccompany.Service.SaldoClienteService;

@Controller
@RequestMapping("/api/SaldoCliente")
public class SaldoClienteController {

	@Autowired
	SaldoClienteService saldoClienteService;
	
	@GetMapping
	@ResponseBody
	public List<SaldoCliente> listaSaldoCliente(){
		return saldoClienteService.allSaldoCliente();
			
	}
	
	@PostMapping(value="/novoSaldoCliente")
	@ResponseBody
	public SaldoCliente SaldoCliente(@RequestBody SaldoCliente saldoCliente) {
		
		return saldoClienteService.save(saldoCliente);

	}
	
	@PutMapping(value="/editarSaldoCliente/{id}")
	@ResponseBody
	public SaldoCliente alterarSaldoClientePorID(@PathVariable SaldoId saldoId, @RequestBody SaldoCliente saldoCliente) {
		return saldoClienteService.editarSaldoCliente(saldoId, saldoCliente);
				
	}
}
