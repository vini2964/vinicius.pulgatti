package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.SaldoId;
import br.com.dbccompany.Service.SaldoIdService;

@Controller
@RequestMapping("/api/SaldoId")
public class SaldoIdController {
	
	@Autowired
	SaldoIdService saldoIdService ;
	
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<SaldoId> listaSaldoId(){
		return saldoIdService.allSaldoId();
			
	}
	
	@PostMapping(value="/novoClientePacotes")
	@ResponseBody
	public SaldoId salvaSaldoId(@RequestBody SaldoId saldoId) {
		
		return saldoIdService.save(saldoId);
		
	}
	
}
