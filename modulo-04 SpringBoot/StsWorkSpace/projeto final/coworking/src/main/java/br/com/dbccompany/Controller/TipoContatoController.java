package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.TipoContatoService;

@Controller
@RequestMapping("/api/TipoContato")
public class TipoContatoController {
	
	@Autowired
	TipoContatoService tipoContatoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<TipoContato> listaTipoContato(){
		return tipoContatoService.allTipoContato();
			
	}
	
	@PostMapping(value="/novoTipoContato")
	@ResponseBody
	public TipoContato salvaTipoContato(@RequestBody TipoContato tipoContato) {
		return tipoContatoService.save(tipoContato);
		
	}
	
	@PutMapping(value="/editarTipoContato/{id}")
	@ResponseBody
	public TipoContato alterarTipoContatoPorID(@PathVariable long id, @RequestBody TipoContato tipoContato) {
		return tipoContatoService.editarTipoContato(id, tipoContato);
				
	}
}
