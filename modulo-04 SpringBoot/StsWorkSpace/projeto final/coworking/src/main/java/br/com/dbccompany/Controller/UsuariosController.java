package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Usuarios;
import br.com.dbccompany.Service.UsuariosService;

@Controller
@RequestMapping("/api/Usuarios")
public class UsuariosController {

	@Autowired
	UsuariosService usuariosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Usuarios> listaUsuarios(){
		return usuariosService.allUsuarios();
			
	}
	
	@PostMapping(value="/novoUsuario")
	@ResponseBody
	public Usuarios salvaUsuarios(@RequestBody Usuarios usuarios) throws Exception{
		return usuariosService.save(usuarios);
		
	}
	
	@PutMapping(value="/editarUsuario/{id}")
	@ResponseBody
	public Usuarios alterarUsuarioPorID(@PathVariable long id, @RequestBody Usuarios Usuarios) {
		return usuariosService.editarUsuarios(id, Usuarios);
				
	}
}
