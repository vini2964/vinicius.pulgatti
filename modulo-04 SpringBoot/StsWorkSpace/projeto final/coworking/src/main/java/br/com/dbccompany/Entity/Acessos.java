package br.com.dbccompany.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Acessos {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
	@GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="IS_ENTRADA")
	private boolean isEntrada;
	
	@Column(name="IS_EXCESSAO")
	private boolean isExcesao;
	
	@Column(name="DATA")
	private Date data;
	
	@ManyToOne
    @JoinColumns({ 
        @JoinColumn(name = "ID_CLIENTE",
                    referencedColumnName = "ID_CLIENTE"),
        @JoinColumn(name = "ID_ESPACOS",
                    referencedColumnName = "ID_ESPACOS")})
    private SaldoCliente saldoCliente;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean getIsEntrada() {
		return isEntrada;
	}

	public void setEntrada(boolean isEntrada) {
		this.isEntrada = isEntrada;
	}

	public boolean getIsExcesao() {
		return isExcesao;
	}

	public void setExcesao(boolean isExcesao) {
		this.isExcesao = isExcesao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public SaldoCliente getSaldoCliente() {
		return saldoCliente;
	}

	public void setSaldoCliente(SaldoCliente saldoCliente) {
		this.saldoCliente = saldoCliente;
	}	
	
	
	
}
