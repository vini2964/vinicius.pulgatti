	package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ClientePacotes {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "CLIENTE_PACOTES_SEQ", sequenceName = "CLIENTE_PACOTES_SEQ")
	@GeneratedValue(generator = "CLIENTE_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	private long quantidade;
	
	@ManyToOne
    @JoinColumn(name = "id_clientes")
	private Clientes clientes;
	
	@ManyToOne
    @JoinColumn(name = "id_pacotes")
	private Pacotes pacotes;
	
	@OneToOne
	@JoinColumn(name = "ID_PAGAMENTOS")
	private Pagamentos pagamentos;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}
	
	

	public Clientes getClientes() {
		return clientes;
	}

	public void setClientes(Clientes clientes) {
		this.clientes = clientes;
	}

	public Pacotes getPacotes() {
		return pacotes;
	}
	public void setPacotes(Pacotes pacotes) {
		this.pacotes = pacotes;
	}

	public Pagamentos getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(Pagamentos pagamentos) {
		this.pagamentos = pagamentos;
	}
	
	
	
	
}
