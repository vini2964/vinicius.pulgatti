package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Clientes {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
	@GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME", nullable = false)
	private String nome;
	
	@Column(name = "CPF", nullable = false, unique = true)
	private long cpf;
	
	//@DateTimeFormat(pattern = "yyyy/MM/dd")
	@Column(name = "DATA_NASCIMENTO",nullable = false)
	private Integer dataNascimento;
	
	@OneToMany(mappedBy = "clientes")
    private List<Contato> contato= new ArrayList<>();
	
	@ManyToOne()
    @JoinColumn(name = "id_contratacao")
	private Contratacao contratacao;
	
	@OneToMany(mappedBy = "clientes")
	private List<ClientePacotes> clientePacotes = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public Integer getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Integer dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public List<Contato> getContato() {
		return contato;
	}

	public void setContato(List<Contato> contato) {
		this.contato = contato;
	}

	public Contratacao getContratacao() {
		return contratacao;
	}

	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}

	public List<ClientePacotes> getClientePacotes() {
		return clientePacotes;
	}

	public void setClientePacotes(List<ClientePacotes> clientePacotes) {
		this.clientePacotes = clientePacotes;
	}
	
	
	
	
}
