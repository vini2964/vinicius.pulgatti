package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
public class Contato {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
	@GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	private String valor;
	
	@ManyToOne
    @JoinColumn(name = "id_Tipo_Contato")
	private TipoContato tipoContato;
	
	@ManyToOne
    @JoinColumn(name = "id_Clientes")
	private Clientes clientes;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public TipoContato getTipoContato() {
		return tipoContato;
	}

	public void setTipoContato(TipoContato tipoContato) {
		this.tipoContato = tipoContato;
	}

	public Clientes getClientes() {
		return clientes;
	}

	public void setClientes(Clientes clientes) {
		this.clientes = clientes;
	}
	
	
}
