package br.com.dbccompany.Entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Contratacao {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
	@GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "QUANTIDADE", nullable = false)
	private int quantidade;
	
	@Column(name = "DESCONTO", nullable = true)
	private double desconto;
	
	@Column(name = "PRAZO", nullable = false)
	private Date prazo;
	
	@ManyToOne
    @JoinColumn(name = "id_Espaco")
	private Espacos espacos;
	
	@OneToMany(mappedBy = "contratacao")
    private List<Clientes> clientes= new ArrayList<>();
	
	@OneToOne
	@JoinColumn(name = "ID_PAGAMENTOS")
	private Pagamentos pagamentos;
	
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_CONTRATACAO")
	private TipoContratacao tipoContratacao;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}


	public double getDesconto() {
		return desconto;
	}


	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}


	public Date getPrazo() {
		return prazo;
	}


	public void setPrazo(Date prazo) {
		this.prazo = prazo;
	}


	public Espacos getEspacos() {
		return espacos;
	}


	public void setEspacos(Espacos espacos) {
		this.espacos = espacos;
	}


	public List<Clientes> getClientes() {
		return clientes;
	}


	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}


	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}


	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}
	 
	 
}
