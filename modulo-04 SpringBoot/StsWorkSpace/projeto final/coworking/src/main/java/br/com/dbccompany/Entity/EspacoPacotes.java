package br.com.dbccompany.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class EspacoPacotes {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "ESPACO_PACOTES_SEQ", sequenceName = "ESPACO_PACOTES_SEQ")
	@GeneratedValue(generator = "ESPACO_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	private long quantidade;
	
	private Date prazo;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_CONTRATACAO", nullable = false)
	private TipoContratacao tipoContratacao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}

	public Date getPrazo() {
		return prazo;
	}

	public void setPrazo(Date prazo) {
		this.prazo = prazo;
	}

	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}
	
	
}
