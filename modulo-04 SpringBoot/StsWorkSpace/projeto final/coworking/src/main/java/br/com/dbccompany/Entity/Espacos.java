package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Espacos {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
	@GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "NOME", nullable = false, unique = true)
	private String nome;
	
	@Column(name = "QTD_PESSOAS", nullable = false)
	private long qtdPessoas;
	
	@Column(name="valor", nullable = false)
	private long valor;
	
	@OneToMany(mappedBy = "espacos")
    private List<Contratacao> contratacao= new ArrayList<>();
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getQtPessoas() {
		return qtdPessoas;
	}

	public void setQtPessoas(long qtdPessoas) {
		this.qtdPessoas = qtdPessoas;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}
	
	
}
