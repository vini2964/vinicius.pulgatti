package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Pacotes {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
	@GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	private long valor;
	
	@OneToMany(mappedBy = "pacotes")
	private List<ClientePacotes> clientePacotes = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public List<ClientePacotes> getClientePacotes() {
		return clientePacotes;
	}

	public void setClientePacotes(List<ClientePacotes> clientePacotes) {
		this.clientePacotes = clientePacotes;
	}
	
	
	
	
	
}
