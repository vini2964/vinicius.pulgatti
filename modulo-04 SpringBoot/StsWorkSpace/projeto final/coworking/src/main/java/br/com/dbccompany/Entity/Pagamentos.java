package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Pagamentos {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
	@GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@OneToOne(mappedBy = "pagamentos")
	@JoinColumn(name = "ID_CLIENTE_PACOTES")
	private ClientePacotes clientePacotes;
	
	@OneToOne(mappedBy = "pagamentos")
	@JoinColumn(name = "ID_CONTRATACAO")
	private Contratacao contratacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_PAGAMENTO")
	private TipoPagamento tipoPagamento;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ClientePacotes getClientePacotes() {
		return clientePacotes;
	}

	public void setClientePacotes(ClientePacotes clientePacotes) {
		this.clientePacotes = clientePacotes;
	}

	public Contratacao getContratacao() {
		return contratacao;
	}

	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	
	
}
