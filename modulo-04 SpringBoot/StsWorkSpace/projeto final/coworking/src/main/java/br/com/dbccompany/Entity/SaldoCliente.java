package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="SALDO_CLIENTE")
public class SaldoCliente {
	
	@EmbeddedId
	private SaldoId saldoId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_CONTRATACAO", nullable = false)
	private TipoContratacao tipoContratacao;
	
	@Column(name="QUANTIDADE", nullable = false)
	private long quantidade;
	
	@Column(name= "VENCIMENTO", nullable = false)
	private Date vencimento;
	
	@OneToMany(mappedBy = "saldoCliente")
	private List<Acessos> acessos = new ArrayList<Acessos>();
	
	
	public SaldoId getSaldoId() {
		return saldoId;
	}

	public void setSaldoId(SaldoId saldoId) {
		this.saldoId = saldoId;
	}

	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public List<Acessos> getAcessos() {
		return acessos;
	}

	public void setAcessos(List<Acessos> acessos) {
		this.acessos = acessos;
	}
	
	
	
	
	
}
