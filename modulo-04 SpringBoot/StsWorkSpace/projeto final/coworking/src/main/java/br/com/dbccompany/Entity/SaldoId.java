package br.com.dbccompany.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SaldoId implements Serializable {
	
	@Column(name = "ID_CLIENTE")
	private Long clienteId;
	
	@Column(name="ID_ESPACOS")
	private Long espacosID;
	
	public SaldoId(Long clienteId, Long espacosID) {
		this.clienteId = clienteId;
		this.espacosID = espacosID;
		
	}
	
	public Long getclienteId() {
		return clienteId;
	}
	
	public Long getespacosID() {
		return espacosID;
	}

}
