package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Usuarios {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
	@GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "EMAIL", unique = true)
	private String email;
	
	@Column(name = "LOGIN", unique = true)
	private String login;
	
	// TODO Criptografar senha  MD5 com no minimo 6 digitos
	@Column(name = "SENHA", unique = true)
	private String senha;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
	
}
