package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoCliente;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Long> {
	
	Acessos findBySaldoCliente (SaldoCliente saldo);
}
