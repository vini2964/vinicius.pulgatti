package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.ClientePacotes;

@Repository
public interface ClientePacotesRepository extends CrudRepository<ClientePacotes, Long> {

	ClientePacotes findByQuantidade (long quantidade);
}
