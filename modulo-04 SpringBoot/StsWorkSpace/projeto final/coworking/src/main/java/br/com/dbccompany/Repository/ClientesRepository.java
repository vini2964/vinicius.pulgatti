package br.com.dbccompany.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Long> {
		
	Clientes findByNome(String nome);
	Clientes findByCpf(long cpf);
	Clientes findBydataNascimento(Date dataNascimento);
	Clientes findBycontato (List<Contato> contato);
}
