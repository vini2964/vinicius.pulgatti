package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Long> {

	Contato findByValor (String valor);
	Contato findByTipoContato(TipoContato tipo);
	List<Contato> findByClientes(Clientes cliente);
}
