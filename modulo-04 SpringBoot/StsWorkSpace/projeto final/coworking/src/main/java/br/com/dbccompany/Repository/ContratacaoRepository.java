package br.com.dbccompany.Repository;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Contratacao;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Long>  {
	
	Contratacao findByQuantidade (long quantidade);
	
	Contratacao findByDesconto (long desconto);
	
	Contratacao findByPrazo (Date prazo);
}
