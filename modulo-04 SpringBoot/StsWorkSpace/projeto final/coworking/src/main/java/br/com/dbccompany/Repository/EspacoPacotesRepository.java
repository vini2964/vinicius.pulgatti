package br.com.dbccompany.Repository;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.EspacoPacotes;

@Repository
public interface EspacoPacotesRepository extends CrudRepository<EspacoPacotes, Long> {
	
	EspacoPacotes findByQuantidade(long quantidade);
	EspacoPacotes findByPrazo(Date prazo);

}
