package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Espacos;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Long> {
	
	Espacos findByNome (String nome);
	Espacos findByQtdPessoas(long qtdPessoas);
	Espacos findByValor(long valor);

}
