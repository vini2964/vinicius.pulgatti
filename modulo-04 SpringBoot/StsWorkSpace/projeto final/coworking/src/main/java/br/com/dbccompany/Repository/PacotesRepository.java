package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Pacotes;

@Repository
public interface PacotesRepository extends CrudRepository<Pacotes, Long> {
	
	Pacotes findByValor (long valor);
	

}
