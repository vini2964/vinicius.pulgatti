package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Pagamentos;


@Repository
public interface PagamentosRepository extends CrudRepository<Pagamentos, Long> {
	


}
