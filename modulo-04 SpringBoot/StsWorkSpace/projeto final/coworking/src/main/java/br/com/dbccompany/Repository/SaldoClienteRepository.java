package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.SaldoCliente;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Long> {
	
	SaldoCliente findByQuantidade(Long quantidade);
}
