package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContato, Long>{
	
	TipoContato findByNome(String nome);
	TipoContato findByContato(Contato contato);
	
}
