package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.Entity.Usuarios;


@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Long> {
	
	Usuarios findByNome(String nome);
	Usuarios findByEmail(String email);
	Usuarios findByLogin(String login);
}
