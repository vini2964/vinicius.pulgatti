package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoId;
import br.com.dbccompany.Repository.AcessosRepository;

@Service
public class AcessosService {
	
	@Autowired
	public  AcessosRepository  acessosRepository;
	
		//Salva
		@Transactional(rollbackFor = Exception.class)
		public Acessos save(Acessos acessos) {
			
			return acessosRepository.save(acessos);
			
		}
		
		//Busca
		@SuppressWarnings("unused")
		public Optional<Acessos> buscarAcessos(long id) {
			return acessosRepository.findById(id);
		}
		
		@Transactional(rollbackFor = Exception.class)
		public Acessos editarAcessos(long id, Acessos acessos) {
			acessos.setId(id);
			return acessosRepository.save(acessos);
		}
		
		//Remove
		@Transactional(rollbackFor = Exception.class)
		public void removerAcessos( Acessos acessos)  {
			acessosRepository.delete(acessos);
		}
		
		//Lista
		public List<Acessos> allAcessos() {
			return (List<Acessos>) acessosRepository.findAll();
		}
}
