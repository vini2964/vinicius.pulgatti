package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.ClientePacotes;
import br.com.dbccompany.Repository.ClientePacotesRepository;

@Service
public class ClientePacotesService {
	
	@Autowired
	public ClientePacotesRepository clientePacotesRepository;
	
	//Salva
	@Transactional(rollbackFor = Exception.class)
	public ClientePacotes save(ClientePacotes clientePacotes) {
		
		return clientePacotesRepository.save(clientePacotes);
		
	}
	
	//Busca
	@SuppressWarnings("unused")
	public Optional<ClientePacotes> buscarClientePacotes(long id) {
		return clientePacotesRepository.findById(id);
	}
	
	//Edita
	@Transactional(rollbackFor = Exception.class)
	public ClientePacotes editarClientePacotes(long id, ClientePacotes clientePacotes) {
		clientePacotes.setId(id);
		return clientePacotesRepository.save(clientePacotes);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerClientePacotes( ClientePacotes clientePacotes)  {
		clientePacotesRepository.delete(clientePacotes);
	}
	
	//Lista
	public List<ClientePacotes> allClientePacotes() {
		return (List<ClientePacotes>) clientePacotesRepository.findAll();
	}
}
