package br.com.dbccompany.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.ContatoRepository;
//clientes.getContato().get(0).getTipoContato() != "email"
@Service
public class ClientesService {
	
	@Autowired
	public ClientesRepository clientesRepository;
	
	@Autowired
	public ContatoRepository contatoRepository;
	
	//Salva
	@Transactional(rollbackFor = Exception.class)
	public Clientes save(Clientes clientes) {
		Contato contato = contatoRepository.findById(clientes.getContato().get(0).getId()).get();
		Contato contato2 = contatoRepository.findById(clientes.getContato().get(1).getId()).get();
		
		String base0 = contato.getTipoContato().getNome().toLowerCase();
		String base1 = contato2.getTipoContato().getNome().toLowerCase();
		
		List<String> tiposContatos = new ArrayList<String>();
		
		tiposContatos.add(base0);
		tiposContatos.add(base1);
			
		if(tiposContatos.contains("email") && tiposContatos.contains("telefone")) {
				
		
					return clientesRepository.save(clientes);
				}
			
			return null;
	}
	
	//Busca
	@SuppressWarnings("unused")
	public Optional<Clientes> buscarClientes(long id) {
		return clientesRepository.findById(id);
	}
	
	//Edita
	@Transactional(rollbackFor = Exception.class)
	public Clientes editarClientes(long id, Clientes clientes) {
		clientes.setId(id);
		return clientesRepository.save(clientes);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerClientes( Clientes clientes)  {
		clientesRepository.delete(clientes);
	}
	
	//Lista
	public List<Clientes> allClientes() {
		return (List<Clientes>) clientesRepository.findAll();
	}
}
