package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ContatoRepository;

@Service
public class ContatoService {
	
	@Autowired
	ContatoRepository contatoRepository;
	
	//Salva
		@Transactional(rollbackFor = Exception.class)
		public Contato save(Contato contato) {
			
			return contatoRepository.save(contato);
			
		}
		
		//Busca
		@SuppressWarnings("unused")
		public Optional<Contato> buscarContato(long id) {
			return contatoRepository.findById(id);
		}
		
		//Edita
		@Transactional(rollbackFor = Exception.class)
		public Contato editarContato(long id, Contato contato) {
			contato.setId(id);
			return contatoRepository.save(contato);
		}
		
		
		//Remove
		@Transactional(rollbackFor = Exception.class)
		public void removerContato( Contato contato)  {
			contatoRepository.delete(contato);
		}
		
		//Lista
		public List<Contato> allContato() {
			return (List<Contato>) contatoRepository.findAll();
		}
}
