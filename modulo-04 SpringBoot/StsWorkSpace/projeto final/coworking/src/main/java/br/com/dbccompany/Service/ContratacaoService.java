package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.TipoContratacao;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.ContratacaoRepository;
import br.com.dbccompany.Repository.EspacosRepository;

@Service
public class ContratacaoService {
	
	@Autowired
	public ContratacaoRepository contratacaoRepository;
	
	@Autowired
	public EspacosRepository espacoRepository;
	
	@Autowired
	public ClientesRepository clienteRepository;
	
	//Salva
	@Transactional(rollbackFor = Exception.class)
	public double save(Contratacao contratacao) {
		if(contratacao.getEspacos().getId() == null) {
		
			contratacao.setEspacos(contratacao.getEspacos());
			espacoRepository.save(contratacao.getEspacos());
		}
		double valor = espacoRepository.findById(contratacao.getEspacos().getId()).get().getValor();
		int qtd = contratacao.getQuantidade();
		TipoContratacao tipoContratacao = contratacao.getTipoContratacao();
		double desconto = contratacao.getDesconto();
		
		contratacaoRepository.save(contratacao);
		 
		 return CustoContratado(valor,qtd,tipoContratacao,desconto);
	
	
	}
	
	//Busca
	@SuppressWarnings("unused")
	public Optional<Contratacao> buscarContratacao(long id) {
		return contratacaoRepository.findById(id);
	}
	
	//Edita
	@Transactional(rollbackFor = Exception.class)
	public Contratacao editarContratacao(long id, Contratacao contratacao) {
		contratacao.setId(id);
		return contratacaoRepository.save(contratacao);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerContratacao( Contratacao contratacao)  {
		contratacaoRepository.delete(contratacao);
	}
	
	//Lista
	public List<Contratacao> allContratacao() {
		return (List<Contratacao>) contratacaoRepository.findAll();
	}
	
	public Double CustoContratado(double valorBase, int quantidade, TipoContratacao tipoContratacao,double desconto) {
        Double custo;
        switch (tipoContratacao) {
            case MINUTO:
            	custo = valorBase * quantidade / 60 - desconto;
                break;
            case HORA:
            	custo = valorBase * quantidade - desconto;
                break;
            case TURNO:
            	custo = valorBase * quantidade * 5 - desconto;
                break;
            case DIARIA:
            	custo = valorBase * quantidade * 8 - desconto;
                break;
            case SEMANA:
            	custo = valorBase * quantidade * 44 - desconto;
                break;
            case MES:
            	custo = valorBase * quantidade * 220 - desconto;
                break;
            default:
            	custo = valorBase * quantidade - desconto;
        }
        return (custo);
    }
}
