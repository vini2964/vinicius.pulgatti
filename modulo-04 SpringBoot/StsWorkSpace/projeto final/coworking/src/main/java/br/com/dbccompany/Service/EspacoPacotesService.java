package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EspacoPacotes;
import br.com.dbccompany.Repository.EspacoPacotesRepository;

@Service
public class EspacoPacotesService {


	@Autowired
	EspacoPacotesRepository espacoPacotesRepository;
	
	//Salva
		@Transactional(rollbackFor = Exception.class)
		public EspacoPacotes save(EspacoPacotes espacoPacotes) {
			
			return espacoPacotesRepository.save(espacoPacotes);
			
		}
		
		//Busca
		@SuppressWarnings("unused")
		public Optional<EspacoPacotes> buscarEspacoPacotes(long id) {
			return espacoPacotesRepository.findById(id);
		}
		
		//Edita
		@Transactional(rollbackFor = Exception.class)
		public EspacoPacotes editarEspacoPacotes(long id, EspacoPacotes espacoPacotes) {
			espacoPacotes.setId(id);
			return espacoPacotesRepository.save(espacoPacotes);
		}
		
		
		//Remove
		@Transactional(rollbackFor = Exception.class)
		public void removerEspacoPacotes( EspacoPacotes espacoPacotes)  {
			espacoPacotesRepository.delete(espacoPacotes);
		}
		
		//Lista
		public List<EspacoPacotes> allEspacoPacotes() {
			return (List<EspacoPacotes>) espacoPacotesRepository.findAll();
		}
}
