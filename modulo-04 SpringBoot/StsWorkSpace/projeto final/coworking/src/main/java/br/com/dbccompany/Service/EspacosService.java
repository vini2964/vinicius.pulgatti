package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Repository.EspacosRepository;

@Service
public class EspacosService {
	
	@Autowired
	public EspacosRepository espacosRepository;
	
	//Salva
	@Transactional(rollbackFor = Exception.class)
	public Espacos save(Espacos espacos) {
		
		return espacosRepository.save(espacos);
		
	}
	
	//Busca
	@SuppressWarnings("unused")
	public Optional<Espacos> buscarEspacos(long id) {
		return espacosRepository.findById(id);
	}
	
	//Edita
	@Transactional(rollbackFor = Exception.class)
	public Espacos editarEspacos(long id, Espacos espacos) {
		espacos.setId(id);
		return espacosRepository.save(espacos);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerEspacos( Espacos espacos)  {
		espacosRepository.delete(espacos);
	}
	
	//Lista
	public List<Espacos> allEspacos() {
		return (List<Espacos>) espacosRepository.findAll();
	}
}
