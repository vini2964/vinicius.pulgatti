package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Repository.PacotesRepository;

@Service
public class PacotesService {
	
	@Autowired
	PacotesRepository pacotesRepository;
	
	//Salva
		@Transactional(rollbackFor = Exception.class)
		public Pacotes save(Pacotes pacote) {
			
			return pacotesRepository.save(pacote);
			
		}
		
		//Busca
		@SuppressWarnings("unused")
		public Optional<Pacotes> buscarPacotes(long id) {
			return pacotesRepository.findById(id);
		}
		
		//Edita
		@Transactional(rollbackFor = Exception.class)
		public Pacotes editarPacotes(long id, Pacotes pacote) {
			pacote.setId(id);
			return pacotesRepository.save(pacote);
		}
		
		
		//Remove
		@Transactional(rollbackFor = Exception.class)
		public void removerPacotes( Pacotes pacote)  {
			pacotesRepository.delete(pacote);
		}
		
		//Lista
		public List<Pacotes> allPacotes() {
			return (List<Pacotes>) pacotesRepository.findAll();
		}
}
