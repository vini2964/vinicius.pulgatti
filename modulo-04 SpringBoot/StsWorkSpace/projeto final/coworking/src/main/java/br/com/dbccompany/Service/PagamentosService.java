package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Repository.PagamentosRepository;
import br.com.dbccompany.Repository.SaldoClienteRepository;


@Service
public class PagamentosService {
	
	@Autowired
	public PagamentosRepository pagamentosRepository;
	
	//@Autowired
	//public SaldoIdRepository saldoIdRepository;
	
	@Autowired
	public SaldoClienteRepository saldoClienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamentos save(Pagamentos pagamentos) {
		
		return pagamentosRepository.save(pagamentos);
		
	}
	
	
	//Salva
	/*@Transactional(rollbackFor = Exception.class)
	public void saldoClienteContratacao(Pagamentos pagamento) {
		SaldoId saldoClienteId = new SaldoId(pagamento.getContratacao().getClientes().get(0).getId(),
		pagamento.getContratacao().getEspacos().getId());
		Boolean saldoClienteNExiste = !saldoIdRepository.existsById(saldoClienteId.getclienteId());
			if (saldoClienteNExiste) {
				SaldoCliente novoSaldoCliente = new SaldoCliente();
				novoSaldoCliente.setSaldoId(saldoClienteId);
				novoSaldoCliente.setQuantidade(pagamento.getContratacao().getQuantidade());
				novoSaldoCliente.setTipoContratacao(pagamento.getContratacao().getTipoContratacao());
				Date date = new Date(0);
				novoSaldoCliente.setVencimento(date);
				saldoClienteRepository.save(novoSaldoCliente);
			} 
	}
	*/
	//Busca
	@SuppressWarnings("unused")
	public Optional<Pagamentos> buscarPagamentos(long id) {
		return pagamentosRepository.findById(id);
	}
	
	//Edita
	@Transactional(rollbackFor = Exception.class)
	public Pagamentos editarPagamentos(long id, Pagamentos pagamento) {
		pagamento.setId(id);
		return pagamentosRepository.save(pagamento);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerPagamentos( Pagamentos pagamento)  {
		pagamentosRepository.delete(pagamento);
	}
	
	//Lista
	public List<Pagamentos> allPagamentos() {
		return (List<Pagamentos>) pagamentosRepository.findAll();
	}
}
