package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoId;
import br.com.dbccompany.Repository.SaldoClienteRepository;

@Service
public class SaldoClienteService {
	
	@Autowired
	public  SaldoClienteRepository  saldoClienteRepository;
	
	//Salva
			@Transactional(rollbackFor = Exception.class)
			public SaldoCliente save(SaldoCliente saldoCliente) {
				
				return saldoClienteRepository.save(saldoCliente);
				
			}
			
			//Busca
			@SuppressWarnings("unused")
			public Optional<SaldoCliente> buscarSaldoCliente(long id) {
				return saldoClienteRepository.findById(id);
			}
			
			@Transactional(rollbackFor = Exception.class)
			public SaldoCliente editarSaldoCliente(SaldoId saldoId, SaldoCliente saldoCliente) {
				saldoId.getclienteId();
				saldoId.getespacosID();
				return saldoClienteRepository.save(saldoCliente);
			}
			
			//Remove
			@Transactional(rollbackFor = Exception.class)
			public void removerSaldoCliente( SaldoCliente saldoCliente)  {
				saldoClienteRepository.delete(saldoCliente);
			}
			
			//Lista
			public List<SaldoCliente> allSaldoCliente() {
				return (List<SaldoCliente>) saldoClienteRepository.findAll();
			}
}
