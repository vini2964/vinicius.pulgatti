package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.SaldoId;
import br.com.dbccompany.Repository.SaldoIdRepository;

@Service
public class SaldoIdService {
	
	@Autowired
	public SaldoIdRepository saldoIdRepository;
	
	//Salva
	@Transactional(rollbackFor = Exception.class)
	public SaldoId save(SaldoId SaldoId) {
		
		return saldoIdRepository.save(SaldoId);

	}
	
	//Busca
	@SuppressWarnings("unused")
	public Optional<SaldoId> buscarSaldoId(long id) {
		return saldoIdRepository.findById(id);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerSaldoId( SaldoId saldoId)  {
		saldoIdRepository.delete(saldoId);
	}
	
	//Lista
	public List<SaldoId> allSaldoId() {
		return (List<SaldoId>) saldoIdRepository.findAll();
	}
}
