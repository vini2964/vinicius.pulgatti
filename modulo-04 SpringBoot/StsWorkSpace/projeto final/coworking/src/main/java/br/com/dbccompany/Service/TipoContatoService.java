package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;

@Service
public class TipoContatoService {

	@Autowired
	TipoContatoRepository tipoContatoRepository;
	
	//Salva
		@Transactional(rollbackFor = Exception.class)
		public TipoContato save(TipoContato tipoContato) {
			
			return tipoContatoRepository.save(tipoContato);
			
		}
		
		//Busca
		@SuppressWarnings("unused")
		public Optional<TipoContato> buscarTipoContato(long id) {
			return tipoContatoRepository.findById(id);
		}
		
		//Edita
		@Transactional(rollbackFor = Exception.class)
		public TipoContato editarTipoContato(long id, TipoContato tipoContato) {
			tipoContato.setId(id);
			return tipoContatoRepository.save(tipoContato);
		}
		
		
		//Remove
		@Transactional(rollbackFor = Exception.class)
		public void removerTipoContato( TipoContato tipoContato)  {
			tipoContatoRepository.delete(tipoContato);
		}
		
		//Lista
		public List<TipoContato> allTipoContato() {
			return (List<TipoContato>) tipoContatoRepository.findAll();
		}
}
