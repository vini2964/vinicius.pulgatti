package br.com.dbccompany.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Usuarios;
import br.com.dbccompany.Repository.UsuariosRepository;



@Service
public class UsuariosService {
	
	@Autowired
	public UsuariosRepository usuariosRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/*public static String criptografar(String senha) throws Exception {
        
		 MessageDigest m = MessageDigest.getInstance("MD5");
        
		 m.update(senha.getBytes("UTF-8"));
		 
        BigInteger senhaCreep = new BigInteger(1, m.digest());
        
		 return senha = String.format("%1$032X", senhaCreep);
	}
	*/
	//Salva
	@Transactional(rollbackFor = Exception.class)
	public Usuarios save(Usuarios usuarios) throws Exception{

		String senha = usuarios.getSenha();
		if(senha.length() < 6) { 
			return null;
		}
		else {
			
			usuarios.setSenha(passwordEncoder.encode(usuarios.getSenha()));
			System.out.println(usuarios);
			return usuariosRepository.save(usuarios);
		}		
	}
	
	//Busca
	@SuppressWarnings("unused")
	public Optional<Usuarios> buscarUsuarios(long id) {
		return usuariosRepository.findById(id);
	}
	
	//Edita
	@Transactional(rollbackFor = Exception.class)
	public Usuarios editarUsuarios(long id, Usuarios usuarios) {
		usuarios.setId(id);
		return usuariosRepository.save(usuarios);
	}
	
	
	//Remove
	@Transactional(rollbackFor = Exception.class)
	public void removerUsuarios( Usuarios usuarios)  {
		usuariosRepository.delete(usuarios);
	}
	
	//Lista
	public List<Usuarios> allUsuarios() {
		return (List<Usuarios>) usuariosRepository.findAll();
	}
}
