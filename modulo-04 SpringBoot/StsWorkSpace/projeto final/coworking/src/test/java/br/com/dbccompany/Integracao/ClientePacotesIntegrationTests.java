package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.ClientePacotes;
import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Repository.ClientePacotesRepository;

public class ClientePacotesIntegrationTests extends CoworkingApplicationTests {
	
	@MockBean
	private ClientePacotesRepository clientePacotesRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		
		
		Clientes cliente = new Clientes();
		cliente.setNome("Marcos");
		cliente.setCpf(54191564);
		cliente.setDataNascimento(981015);
		
		Pacotes pacote = new Pacotes();
		pacote.setValor(10);
		
		ClientePacotes cp = new ClientePacotes();
		cp.setClientes(cliente);
		cp.setPacotes(pacote);
		cp.setQuantidade(3);
		
		Mockito.when(clientePacotesRepository.findByQuantidade(cp.getQuantidade())).thenReturn(cp);
		
	}
	
	
	@Test
	public void CriarClientePacote() {
		long qtd = 3;
		
		ClientePacotes found = clientePacotesRepository.findByQuantidade(qtd);
		
		assertThat(found.getQuantidade()).isEqualTo(qtd);
	}
}
