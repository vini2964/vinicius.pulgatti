package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplication;
import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import br.com.dbccompany.Repository.TipoContatoRepository;


public class ClientesIntegrationTests extends CoworkingApplicationTests {
	
	
	@MockBean
	private ClientesRepository clientesRepository;
	
	@MockBean
	private ContatoRepository contatoRepository;
	
	@MockBean
	private TipoContatoRepository tipoContatoRepository;
			
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		
		//Criação Cliente
		TipoContato tipo = new TipoContato();
		tipo.setNome("telefone");
		//TipoContato tipo2 = new TipoContato();
		//tipo2.setNome("email");
		Contato contato = new Contato();
		contato.setValor("51681354");
		Contato contato2 = new Contato();
		contato2.setValor("51681354");
		contato.setTipoContato(tipo);
		//contato2.setTipoContato(tipo2);
		List<Contato> listaContato = new ArrayList();
		listaContato.add(contato);
		listaContato.add(contato2);
		Clientes cliente = new Clientes();
		cliente.setContato(listaContato);
		cliente.setNome("Marcos");
		cliente.setCpf(54191564);
		cliente.setDataNascimento(981015);
		
		
		Mockito.when(clientesRepository.findByNome(cliente.getNome())).thenReturn(cliente);
		Mockito.when(contatoRepository.findByTipoContato(tipo)).thenReturn(contato);
		//Mockito.when(contatoRepository.findByTipoContato(tipo2)).thenReturn(contato2);
		Mockito.when(tipoContatoRepository.findByContato(contato)).thenReturn(tipo);
		//Mockito.when(tipoContatoRepository.findByContato(contato2)).thenReturn(tipo2);
		Mockito.when(tipoContatoRepository.findByContato(contato)).thenReturn(tipo);
	}
	
	@Test
	public void VerificaNomeCliente() {
		String nome = "Marcos";
		
		Clientes found = clientesRepository.findByNome(nome);
		
		assertThat(found.getNome()).isEqualTo(nome);
	}
	
	
	@Test
	public void VerificaClienteComDoisTiposContatos() {
		String nome = "telefone";
		String nomeC = "Marcos";
				
		Clientes found = clientesRepository.findByNome(nomeC);
		
		assertThat(found.getContato().get(0).getTipoContato().getNome()).isEqualTo(nome);
		assertEquals(2,found.getContato().size());
		
	}
}
