package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ContatoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ContatoIntegrationTests {
	
	@MockBean
	private ContatoRepository contatoRepository ;
			
	@Autowired
	ApplicationContext applicationcontext;
	
	@Before
	public void setUp() {
		Contato contato = new Contato(); 
		contato.setValor("5151");
		
		Mockito.when(contatoRepository.findByValor(contato.getValor())).thenReturn(contato);
		
	}
	
	@Test
	public void ContatoValorR3() {
		String valor = "5151";
		Contato found = contatoRepository.findByValor(valor);
				
		assertThat(found.getValor()).isEqualTo(valor);
				
				
	}
}
