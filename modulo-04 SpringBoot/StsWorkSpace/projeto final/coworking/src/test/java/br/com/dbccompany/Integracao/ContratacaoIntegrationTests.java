package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Repository.ContratacaoRepository;


public class ContratacaoIntegrationTests extends CoworkingApplicationTests {

	@MockBean
	private ContratacaoRepository ContratacaoRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Contratacao contrat = new Contratacao();
		contrat.setQuantidade(2);
		//contrat.setPrazo(10.10.1998);
		
		Mockito.when(ContratacaoRepository.findByQuantidade(contrat.getQuantidade())).thenReturn(contrat);
		
	}
	
	@Test
	public void DescontoOpcional() {
		long qtd = 2;
		
		Contratacao found = ContratacaoRepository.findByQuantidade(qtd);
		
		assertThat(found.getQuantidade()).isEqualTo(qtd);
	}
}
