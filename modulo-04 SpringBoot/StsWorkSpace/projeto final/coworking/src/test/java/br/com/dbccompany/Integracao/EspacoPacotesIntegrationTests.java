package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.EspacoPacotes;
import br.com.dbccompany.Repository.EspacoPacotesRepository;


public class EspacoPacotesIntegrationTests extends CoworkingApplicationTests{
	
	@MockBean
	private EspacoPacotesRepository espacoPacotesRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		
		EspacoPacotes espP = new EspacoPacotes();
		espP.setQuantidade(2);
		
		Mockito.when(espacoPacotesRepository.findByQuantidade(espP.getQuantidade())).thenReturn(espP);
		
		}
	
	
	@Test
	public void TestaCriacaoPacote() {
			long qtd = 2;
			
			EspacoPacotes found = espacoPacotesRepository.findByQuantidade(qtd);
		
		assertThat(found.getQuantidade()).isEqualTo(qtd);
	}
}
