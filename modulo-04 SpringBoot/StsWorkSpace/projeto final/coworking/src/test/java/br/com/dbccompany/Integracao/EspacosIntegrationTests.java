package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Repository.EspacosRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EspacosIntegrationTests {
	
	@MockBean
	private EspacosRepository espacosRepository;
			
	@Autowired
	ApplicationContext applicationcontext;
	
	@Before
	public void setUp() {
		Espacos espacos = new Espacos(); 
		espacos.setNome("1");
		

		Mockito.when(espacosRepository.findByNome(espacos.getNome())).thenReturn(espacos);
		
	}
	
	@Test
	public void AchaEspaco() {
		String nome = "1";
		Espacos found = espacosRepository.findByNome(nome);
		
		assertThat(found.getNome()).isEqualTo(nome);	
				
	}
}
