package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.ClientePacotes;
import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.PacotesRepository;

public class PacotesIntegrationTests extends CoworkingApplicationTests {
	
	@MockBean
	private PacotesRepository pacotesRepository;
	
	@Autowired
	ApplicationContext context;
		
	@Before
	public void setUp() {
			
	
			//Criação Cliente
			TipoContato tipo = new TipoContato();
			tipo.setNome("telefone");
			TipoContato tipo2 = new TipoContato();
			tipo2.setNome("email");
			Contato contato = new Contato();
			contato.setValor("51681354");
			Contato contato2 = new Contato();
			contato2.setValor("51681354");
			contato.setTipoContato(tipo);
			contato2.setTipoContato(tipo2);
			List<Contato> listaContato = new ArrayList();
			listaContato.add(contato);
			listaContato.add(contato2);
			
			Clientes cliente = new Clientes();
			cliente.setContato(listaContato);
			cliente.setNome("Marcos");
			cliente.setCpf(54191564);
			cliente.setDataNascimento(981015);
			
			Clientes cliente2 = new Clientes();
			cliente2.setContato(listaContato);
			cliente2.setNome("Marcos");
			cliente2.setCpf(54191564);
			cliente2.setDataNascimento(981015);
			
			//Criacao ClientePacote
			ClientePacotes clientP = new ClientePacotes();
			clientP.setClientes(cliente);
			clientP.setClientes(cliente2);
			List<ClientePacotes> clientPList = new ArrayList();
			clientPList.add(clientP);
			
			//Criacao Pacote 
			Pacotes pacote = new Pacotes();
			pacote.setValor(23);
			pacote.setClientePacotes(clientPList);
			
			Mockito.when(pacotesRepository.findByValor(23)).thenReturn(pacote);
			
	}
	
}
