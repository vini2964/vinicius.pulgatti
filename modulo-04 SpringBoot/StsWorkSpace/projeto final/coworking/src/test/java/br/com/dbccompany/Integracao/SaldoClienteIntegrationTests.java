package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Repository.SaldoClienteRepository;

public class SaldoClienteIntegrationTests extends CoworkingApplicationTests {
	
	@MockBean
	private SaldoClienteRepository saldoClienteRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		SaldoCliente saldo = new SaldoCliente();
		saldo.setQuantidade(3);
		
		
		Mockito.when(saldoClienteRepository.findByQuantidade(saldo.getQuantidade())).thenReturn(saldo);
		
	}
	
/*	@Test
	public void SalvarSaldo() {
		int qtd = 3;
		
		SaldoCliente found = saldoClienteRepository.findByQuantidade(3);
		
		assertThat(found.getQuantidade()).isEqualTo(qtd);
		
	}*/
}
