package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest

public class TipoContatoIntegrationTests {
	
	@MockBean
	private TipoContatoRepository tipoContatoRepository;
			
	@Autowired
	ApplicationContext applicationcontext;
	
	@Before
	public void setUp() {
		TipoContato tipoContato = new TipoContato(); 
		tipoContato.setNome("telefone");
		
		Mockito.when(tipoContatoRepository.findByNome(tipoContato.getNome())).thenReturn(tipoContato);
		
	}
	
	@Test
	public void NomenclaturaTipagemR2() {
		String nome = "telefone";
		TipoContato found = tipoContatoRepository.findByNome(nome);
				
		assertThat(found.getNome()).isEqualTo(nome);
				
				
	}
}
