package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.Usuarios;
import br.com.dbccompany.Repository.UsuariosRepository;

public class UsuariosIntegrationTests extends CoworkingApplicationTests {

	@MockBean
	private UsuariosRepository usuariosRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		
		
		Usuarios user = new Usuarios();
		user.setEmail("vini@vini");
		user.setLogin("vinicius");
		user.setNome("pulgatti");
		user.setSenha("98419");
		
		Mockito.when(usuariosRepository.findByEmail(user.getEmail())).thenReturn(user);

	}	
		
	@Test
	public void TestaCriacaoUsuario() {
		String email = "vini@vini";
		
		Usuarios found = usuariosRepository.findByEmail(email);
		
		assertThat(found.getEmail()).isEqualTo(email);
		
	}
		
}


