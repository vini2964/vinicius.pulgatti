package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.BairroService;
import br.com.dbccompany.entity.Bairro;

@Controller
@RequestMapping("/api/Bairro")
public class BairroController {

	@Autowired
	BairroService bairroService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Bairro> listaBairro(){
		return bairroService.allBairros();
		
	}
	
	@PostMapping(value="/novoBairro")
	@ResponseBody
	public Bairro novoBairro(@RequestBody Bairro bairro) {
		return bairroService.save(bairro);
		
	}
	
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Bairro alterarBairroId(@PathVariable long id, @RequestBody Bairro bairro) {
		return bairroService.editarBairro(id, bairro);
				
	}
}
