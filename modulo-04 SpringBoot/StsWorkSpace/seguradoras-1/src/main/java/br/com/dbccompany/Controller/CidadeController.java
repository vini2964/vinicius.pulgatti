package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.CidadeService;
import br.com.dbccompany.entity.Cidade;

@Controller
@RequestMapping("/api/Cidade")
public class CidadeController {
	
	@Autowired
	CidadeService cidadeService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Cidade> listaCidade(){
		return cidadeService.allcidades();
		
	}
	
	@PostMapping(value="/novaCidade")
	@ResponseBody
	public Cidade novaCidade(@RequestBody Cidade cidade) {
		return cidadeService.save(cidade);
		
	}
	
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Cidade alterarCidadeId(@PathVariable long id, @RequestBody Cidade cidade) {
		return cidadeService.editarCidade(id, cidade);
				
	}
}

