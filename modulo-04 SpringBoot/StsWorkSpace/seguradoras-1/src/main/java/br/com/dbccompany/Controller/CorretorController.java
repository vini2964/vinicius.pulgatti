package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.CorretorService;
import br.com.dbccompany.entity.Corretor;

@Controller
@RequestMapping("/api/Corretor")
public class CorretorController {
	
	@Autowired
	CorretorService corretorService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Corretor> listaCorretor(){
		return corretorService.allCorretores();
		
		
	}
	
	@PostMapping(value="/novoCorretor")
	@ResponseBody
	public Corretor novoCorretor(@RequestBody Corretor corretor) {
		return corretorService.save(corretor);
		
	}

		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Corretor alterarCorretorID(@PathVariable long id, @RequestBody Corretor corretor) {
		return corretorService.editarCorretor(id, corretor);
				
	}
}
