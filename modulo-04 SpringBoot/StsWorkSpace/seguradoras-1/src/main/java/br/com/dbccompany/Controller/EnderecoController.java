package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.EnderecoService;
import br.com.dbccompany.entity.Endereco;

@Controller
@RequestMapping("/api/Endereco")
public class EnderecoController {
	
	
	@Autowired
	EnderecoService enderecoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Endereco> listaEndereco(){
		return enderecoService.allEnderecos();
		
		
	}
	
	@PostMapping(value="/novoEndereco")
	@ResponseBody
	public Endereco novoEndereco(@RequestBody Endereco endereco) {
		return enderecoService.save(endereco);
		
	}

		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Endereco alterarEnderecoID(@PathVariable long id, @RequestBody Endereco endereco) {
		return enderecoService.editarEndereco(id, endereco);
				
	}
}
