package br.com.dbccompany.Controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.EnderecoSeguradoraService;
import br.com.dbccompany.entity.EnderecoSeguradora;
@Controller
@RequestMapping("/api/EnderecoSeguradora")
public class EnderecoSeguradoraController {
		
		@Autowired
		EnderecoSeguradoraService enderecoSeguradoraService;
		
		@GetMapping(value = "/")
		@ResponseBody
		public List<EnderecoSeguradora> listaEnderecoSeguradora(){
			return enderecoSeguradoraService.allSegEnd() ;
			
					}
		
		
		@PostMapping(value="/novoEnderecoSeg")
		@ResponseBody
		public EnderecoSeguradora novaCidade(@RequestBody EnderecoSeguradora enderecoSeguradora) {
			
			return enderecoSeguradoraService.save(enderecoSeguradora);
			
		}
		
			
		@PutMapping(value="/editar/{id}")
		@ResponseBody
		public EnderecoSeguradora alterarCidadeId(@PathVariable long id, @RequestBody EnderecoSeguradora enderecoSeguradora) {
			return enderecoSeguradoraService.editarEnderecoSeguradorasId(id, enderecoSeguradora);
					
		}
	
}
