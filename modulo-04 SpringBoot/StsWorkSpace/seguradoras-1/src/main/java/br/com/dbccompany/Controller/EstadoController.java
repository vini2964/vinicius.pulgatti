package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.EstadoService;
import br.com.dbccompany.entity.Estado;

@Controller
@RequestMapping("/api/estado")
public class EstadoController {
	
	
	@Autowired
	EstadoService estadoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Estado> listaEstado(){
		return estadoService.allEstados();
		
		
	}
	
	@PostMapping(value="/novoEstado")
	@ResponseBody
	public Estado novoEstado(@RequestBody Estado estado) {
		return estadoService.save(estado);
		
	}
	
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Estado alterarEstadoPorId(@PathVariable long id, @RequestBody Estado estado) {
		return estadoService.editarEstado(id, estado);
				
	}
}
