package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.PessoasService;
import br.com.dbccompany.entity.Pessoas;

@Controller
@RequestMapping("/api/Pessoas")
public class PessoasController {
	
	@Autowired
	PessoasService pessoasService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Pessoas> listaPessoas(){
		return pessoasService.allPessoas();
		
		
	}
	
	@PostMapping(value="/novaPessoa")
	@ResponseBody
	public Pessoas novoServicos(@RequestBody Pessoas pessoas) {
		return pessoasService.save(pessoas);
		
	}
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Pessoas alterarServicosPorCodigo(@PathVariable long id, @RequestBody Pessoas pessoas) {
		return pessoasService.editarPessoa(id, pessoas);
				
	}
}
