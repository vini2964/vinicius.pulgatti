package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.SeguradoraService;
import br.com.dbccompany.entity.Seguradora;


@Controller
@RequestMapping("/api/Seguradora")
public class SeguradoraController {
	
	@Autowired
	SeguradoraService seguradoraService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Seguradora> listaSeguradora(){
		return seguradoraService.allSeguradora();
		
		
	}
	
	@PostMapping(value="/novaSeguradora")
	@ResponseBody
	public Seguradora novaSeguradora(@RequestBody Seguradora seguradora) {
		return seguradoraService.save(seguradora);
		
	}
	
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Seguradora alterarSeguradoraId(@PathVariable long id, @RequestBody Seguradora seguradora) {
		return seguradoraService.editarSeguradora(id, seguradora);
				
	}
}
