package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.SeguradosService;
import br.com.dbccompany.entity.Segurados;

@Controller
@RequestMapping("/api/Segurados")
public class SeguradosController {
	@Autowired
	SeguradosService seguradosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Segurados> listaServicos(){
		return seguradosService.allSegurados();
		
		
	}
	
	@PostMapping(value="/novoSegurados")
	@ResponseBody
	public Segurados novoServicos(@RequestBody Segurados segurados) {
		return seguradosService.save(segurados);
		
	}
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Segurados alterarSeguradosId(@PathVariable long id, @RequestBody Segurados segurados) {
		return seguradosService.editarSeguradosId(id, segurados);
				
	}
}
