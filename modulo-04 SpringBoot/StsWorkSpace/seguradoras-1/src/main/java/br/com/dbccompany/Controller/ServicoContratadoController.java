package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.ServicoContratatoService;
import br.com.dbccompany.entity.ServicoContratado;

@Controller
@RequestMapping("/api/ServicoContratado")
public class ServicoContratadoController {
	
	@Autowired
	ServicoContratatoService servicoContratatoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<ServicoContratado> listaServicos(){
		return servicoContratatoService.allServicoContratado();
		
		
	}
	
	@PostMapping(value="/novoServicoContratado")
	@ResponseBody
	public ServicoContratado novoServicoContratado(@RequestBody ServicoContratado servicoContratado) {
		return servicoContratatoService.save(servicoContratado);
		
	}
	
		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public ServicoContratado alterarServicoContratadoID(@PathVariable long id, @RequestBody ServicoContratado servicoContratado) {
		return servicoContratatoService.editarServicoContratado(id, servicoContratado);
				
	}
}
