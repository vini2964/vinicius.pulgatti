package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Services.ServicosService;
import br.com.dbccompany.entity.Servicos;

@Controller
@RequestMapping("/api/Servicos")
public class ServicosController {
	
	@Autowired
	ServicosService servicosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Servicos> listaServicos(){
		return servicosService.allServicos();
		
		
	}
	
	@PostMapping(value="/novoServico")
	@ResponseBody
	public Servicos novoServicos(@RequestBody Servicos servicos) {
		return servicosService.save(servicos);
		
	}
	

		
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Servicos alterarServicosPorCodigo(@PathVariable long id, @RequestBody Servicos servicos) {
		return servicosService.editarServicos(id, servicos);
				
	}
	
}
