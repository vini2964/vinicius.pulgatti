package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Bairro;
import br.com.dbccompany.entity.Cidade;

@Repository
public interface BairroRepository extends CrudRepository<Bairro, Long>  {
	
	Bairro findByNome(String nome);	
	List<Bairro> findBycidade(Cidade cidade);



}