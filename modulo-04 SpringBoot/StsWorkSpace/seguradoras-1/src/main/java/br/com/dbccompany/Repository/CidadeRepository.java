package br.com.dbccompany.Repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;

@Repository
public interface CidadeRepository extends CrudRepository<Cidade, Long>  {
	
	Cidade findByNome(String nome);	
	Cidade findByestado(Estado estado);


}