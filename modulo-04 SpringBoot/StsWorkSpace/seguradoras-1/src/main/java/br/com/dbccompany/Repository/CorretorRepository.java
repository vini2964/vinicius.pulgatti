package br.com.dbccompany.Repository;



import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Corretor;


@Repository
public interface CorretorRepository extends PessoasRepository <Corretor>  {
	
	Corretor findByCargo(String cargo);	

}