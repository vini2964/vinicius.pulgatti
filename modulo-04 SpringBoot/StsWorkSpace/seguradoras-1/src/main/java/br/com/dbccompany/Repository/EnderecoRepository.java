package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Endereco;
import br.com.dbccompany.entity.Pessoas;


@Repository
public interface EnderecoRepository <E extends Endereco> extends CrudRepository<E, Long>  {
	
	E findByLogradouro(String logradouro);	
	E findByNumero(long numero);	
	E findByComplemento(String complemento);
	List<E> findBypessoa(List<Pessoas> pessoa);

}