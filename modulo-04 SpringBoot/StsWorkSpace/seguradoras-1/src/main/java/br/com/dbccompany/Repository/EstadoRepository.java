package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;

@Repository
public interface EstadoRepository extends CrudRepository<Estado, Long>  {
	
	Estado findByNome(String nome);	
	Estado findByCidade(List<Cidade> cidade);
}
