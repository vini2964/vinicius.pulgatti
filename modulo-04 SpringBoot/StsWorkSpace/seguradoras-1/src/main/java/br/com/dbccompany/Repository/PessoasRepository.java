package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Pessoas;

@Repository
public interface PessoasRepository <E extends Pessoas>extends CrudRepository<E, Long>  {
	
	E findByNome(String nome);	
	E findByCpf(long cpf);
	E findByMae(String mae);
	E findByPai(String pai);
	E findByEmail(String email);
}
