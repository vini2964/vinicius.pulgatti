package br.com.dbccompany.Repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Seguradora;

@Repository
public interface SeguradoraRepository extends CrudRepository<Seguradora, Long>  {
	
	Seguradora findByNome(String nome);
	Seguradora findByCnpj(long cnpj);




}