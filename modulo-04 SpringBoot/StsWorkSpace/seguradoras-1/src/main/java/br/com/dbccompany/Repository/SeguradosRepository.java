package br.com.dbccompany.Repository;



import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.entity.ServicoContratado;


@Repository
public interface SeguradosRepository extends PessoasRepository <Segurados> {

	List<Segurados> findByservicosContratados(List<ServicoContratado> servicosContratados);
	
}