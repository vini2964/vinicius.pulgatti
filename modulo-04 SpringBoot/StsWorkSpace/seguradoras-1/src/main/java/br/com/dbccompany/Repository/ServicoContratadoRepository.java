package br.com.dbccompany.Repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.ServicoContratado;

@Repository
public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long>  {
	
	ServicoContratado findBydescricao(String  descricao);
	ServicoContratado findByValor(long valor);




}
