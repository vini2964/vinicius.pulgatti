package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dbccompany.entity.Servicos;

@Repository
public interface ServicosRepository extends CrudRepository<Servicos, Long>  {
	
	Servicos findByNome(String  nome);
	Servicos findByDescricao(String descricao);



}
