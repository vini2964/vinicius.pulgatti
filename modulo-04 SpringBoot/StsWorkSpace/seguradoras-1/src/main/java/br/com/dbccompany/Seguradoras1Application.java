package br.com.dbccompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Seguradoras1Application {

	public static void main(String[] args) {
		SpringApplication.run(Seguradoras1Application.class, args);
	}

}
