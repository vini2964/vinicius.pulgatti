package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.BairroRepository;
import br.com.dbccompany.entity.Bairro;


@Service
public class BairroService {

	@Autowired
	public BairroRepository bairroRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro save(Bairro bairro) {
		
		return bairroRepository.save(bairro);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Bairro> buscarBairro(long id) {
		return bairroRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro editarBairro(long id, Bairro bairro) {
		bairro.setId(id);
		return bairroRepository.save(bairro);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerBairro( Bairro bairro)  {
		bairroRepository.delete(bairro);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void removerBairroId(long id, Bairro bairro) {
		bairro.setId(id);
		bairroRepository.deleteById(id);
	}
	public List<Bairro> allBairros() {
		return (List<Bairro>) bairroRepository.findAll();
	}
	
	
	
	
}
