package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.dbccompany.Repository.CidadeRepository;

import br.com.dbccompany.entity.Cidade;


@Service
public class CidadeService {

	@Autowired
	public CidadeRepository cidadeRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade save(Cidade cidade) {
		
		return cidadeRepository.save(cidade);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Cidade> buscarCidade(long id) {
		return cidadeRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade editarCidade(long id, Cidade cidade) {
		cidade.setId(id);
		return cidadeRepository.save(cidade);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerCidade( Cidade cidade)  {
		cidadeRepository.delete(cidade);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void removerCidadeId(long id, Cidade cidade) {
		cidade.setId(id);
		cidadeRepository.deleteById(id);
	}
	public List<Cidade> allcidades() {
		return (List<Cidade>) cidadeRepository.findAll();
	}
	
	
	
	
}
