package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.CorretorRepository;
import br.com.dbccompany.entity.Corretor;

@Service
public class CorretorService {
	
	@Autowired
	public CorretorRepository corretorRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor save(Corretor corretor) {
		
		return corretorRepository.save(corretor);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Corretor> buscarCorretor(long id) {
		return corretorRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor editarCorretor(long id, Corretor corretor) {
		corretor.setId(id);
		return corretorRepository.save(corretor);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerCorretor( Corretor corretor)  {
		corretorRepository.delete(corretor);
	}
	
	public List<Corretor> allCorretores() {
		return (List<Corretor>) corretorRepository.findAll();
	}
	
	
	
}
