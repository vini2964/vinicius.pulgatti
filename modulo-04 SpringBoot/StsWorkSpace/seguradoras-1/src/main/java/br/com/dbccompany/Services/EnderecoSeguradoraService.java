package br.com.dbccompany.Services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.EnderecoSeguradoraRepository;
import br.com.dbccompany.entity.EnderecoSeguradora;
import br.com.dbccompany.entity.Estado;


@Service
public class EnderecoSeguradoraService {
	
	@Autowired
	public EnderecoSeguradoraRepository  enderecoSeguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradora save(EnderecoSeguradora enderecoSeguradora) {
		
		return enderecoSeguradoraRepository.save(enderecoSeguradora);
		
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradora editarEnderecoSeguradorasId(Long id, EnderecoSeguradora enderecoSeguradora) {
		enderecoSeguradora.setId(id);
		return enderecoSeguradoraRepository.save(enderecoSeguradora);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerSegurados( EnderecoSeguradora enderecoSeguradora)  {
		enderecoSeguradoraRepository.delete(enderecoSeguradora);
	}
	
	public List<EnderecoSeguradora> allSegEnd() {
		return (List<EnderecoSeguradora>) enderecoSeguradoraRepository.findAll();
	}
	
}
