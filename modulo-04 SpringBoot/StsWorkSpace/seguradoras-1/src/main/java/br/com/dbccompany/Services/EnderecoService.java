package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Repository.EnderecoRepository;
import br.com.dbccompany.entity.Endereco;

@Service
public class EnderecoService {
	
	@Autowired
	public EnderecoRepository<Endereco> enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco save(Endereco endereco) {
		
		return enderecoRepository.save(endereco);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Endereco> buscarEndereco(long id) {
		return enderecoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editarEndereco(long id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerEndereco( Endereco endereco)  {
		enderecoRepository.delete(endereco);
	}
	
	public List<Endereco> allEnderecos() {
		return (List<Endereco>) enderecoRepository.findAll();
	}
}
