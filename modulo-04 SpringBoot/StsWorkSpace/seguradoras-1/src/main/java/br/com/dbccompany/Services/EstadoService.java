package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Repository.EstadoRepository;
import br.com.dbccompany.entity.Estado;

@Service
public class EstadoService {
	
	@Autowired
	public EstadoRepository estadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Estado save(Estado estado) {
		
		return estadoRepository.save(estado);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Estado> buscarEstado(long id) {
		return estadoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Estado editarEstado(long id, Estado estado) {
		estado.setId(id);
		return estadoRepository.save(estado);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerEstado( Estado estado)  {
		estadoRepository.delete(estado);
	}
	
	public List<Estado> allEstados() {
		return (List<Estado>) estadoRepository.findAll();
	}
}
