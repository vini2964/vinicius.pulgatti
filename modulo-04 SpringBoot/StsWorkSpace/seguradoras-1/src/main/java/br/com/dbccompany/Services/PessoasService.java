package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.PessoasRepository;
import br.com.dbccompany.entity.Pessoas;

@Service
public class PessoasService {

	@Autowired
	public PessoasRepository<Pessoas> pessoasRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas save(Pessoas pessoas) {
		
		return pessoasRepository.save(pessoas);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Pessoas> buscarPessoas(long id) {
		return pessoasRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas editarPessoa(long id, Pessoas pessoas) {
		pessoas.setId(id);
		return pessoasRepository.save(pessoas);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerEstado( Pessoas pessoas)  {
		pessoasRepository.delete(pessoas);
	}
	
	public List<Pessoas> allPessoas() {
		return (List<Pessoas>) pessoasRepository.findAll();
	}
}
