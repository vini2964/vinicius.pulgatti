package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Repository.SeguradoraRepository;
import br.com.dbccompany.entity.Seguradora;

@Service
public class SeguradoraService {
	
	@Autowired
	public SeguradoraRepository  seguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora save(Seguradora seguradora) {
		
		return seguradoraRepository.save(seguradora);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Seguradora> buscarSeguradora(long id) {
		return seguradoraRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora editarSeguradora(long id, Seguradora seguradora) {
		seguradora.setId(id);
		return seguradoraRepository.save(seguradora);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerSeguradora( Seguradora seguradora)  {
		seguradoraRepository.delete(seguradora);
	}
	
	public List<Seguradora> allSeguradora() {
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
}
