package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.SeguradosRepository;
import br.com.dbccompany.entity.Segurados;

@Service
public class SeguradosService {
	

	@Autowired
	public SeguradosRepository  seguradosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados save(Segurados segurados) {
		
		return seguradosRepository.save(segurados);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Segurados> buscarSegurados(long id) {
		return seguradosRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados editarSeguradosId(Long id, Segurados segurados) {
		segurados.setId(id);
		return seguradosRepository.save(segurados);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerSegurados( Segurados segurados)  {
		seguradosRepository.delete(segurados);
	}
	
	public List<Segurados> allSegurados() {
		return (List<Segurados>) seguradosRepository.findAll();
	}
}
