package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.CorretorRepository;
import br.com.dbccompany.Repository.SeguradosRepository;
import br.com.dbccompany.Repository.ServicoContratadoRepository;
import br.com.dbccompany.entity.Corretor;
import br.com.dbccompany.entity.Segurados;
import br.com.dbccompany.entity.ServicoContratado;

@Service
public class ServicoContratatoService {
	
	
	@Autowired
	public ServicoContratadoRepository  servicoContratadoRepository;
	
	@Autowired
	public CorretorRepository  corretorRepository;
	
	@Autowired
	public SeguradosRepository  seguradosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado save(ServicoContratado servico) {
		Corretor corretor = corretorRepository.findById(servico.getCorretor().getId()).get();
		Segurados segurado = seguradosRepository.findById(servico.getSegurados().getId()).get();
		segurado.setQtdServicos(segurado.getQtdServicos() + 1);
		corretor.setComissao(corretor.getComissao() + (servico.getValor() * 0.1));
		servico.setCorretor(corretor);
		servico.setSegurados(segurado);
		return servicoContratadoRepository.save(servico);
		

	}
	
	@SuppressWarnings("unused")
	public Optional<ServicoContratado> buscarServicoContratado(long id) {
		return servicoContratadoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado editarServicoContratado(long id,ServicoContratado servicoContratado) {
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerServicoContratado( ServicoContratado servicoContratado)  {
		servicoContratadoRepository.delete(servicoContratado);
	}
	
	public List<ServicoContratado> allServicoContratado() {
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}
	
	
}
