package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Repository.ServicosRepository;
import br.com.dbccompany.entity.Servicos;

@Service
public class ServicosService {
	
	@Autowired
	public ServicosRepository  servicosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos save(Servicos servicos) {
		
		return servicosRepository.save(servicos);
		
	}
	
	@SuppressWarnings("unused")
	public Optional<Servicos> buscarServicos(long id) {
		return servicosRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos editarServicos(long id, Servicos servicos) {
		servicos.setId(id);
		return servicosRepository.save(servicos);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void removerServicos( Servicos servicos)  {
		servicosRepository.delete(servicos);
	}
	
	public List<Servicos> allServicos() {
		return (List<Servicos>) servicosRepository.findAll();
	}
}
