package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@SequenceGenerator(allocationSize = 1, name = "BAIRRO_SEQ", sequenceName = "BAIRRO_SEQ")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Bairro {
	
	@Id
	@GeneratedValue(generator = "BAIRRO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME", nullable = false, unique = true)
	private String nome;
	
	@ManyToOne
    @JoinColumn(name = "id_cidade")
	private Cidade cidade;
	
	
	@OneToMany(mappedBy = "bairro")
    private List<Endereco> endereco = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Endereco> getEndereco() {
		return endereco;
	}

	public void setEndereco(List<Endereco> endereco) {
		this.endereco = endereco;
	}

	
	
	
}
