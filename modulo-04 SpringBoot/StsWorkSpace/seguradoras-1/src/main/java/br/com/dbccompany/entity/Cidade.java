package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@SequenceGenerator(allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Cidade {
	
	@Id
	@GeneratedValue(generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME",nullable = false, unique = true )
	private String nome;
	
	@ManyToOne
    @JoinColumn(name = "id_estado")
	private Estado estado;
	
	@OneToMany(mappedBy = "cidade")
    private List<Bairro> bairro = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Bairro> getBairro() {
		return bairro;
	}

	public void setBairro(List<Bairro> bairro) {
		this.bairro = bairro;
	}
	
	
}
