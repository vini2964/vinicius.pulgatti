package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "id_pessoa")
public class Corretor extends Pessoas {


	@Column(name = "CARGO",nullable = false, unique = true )
	private String cargo;
	
	@Column(name = "COMISSAO",nullable = false, unique = true )
	private double comissao;
	
	@OneToMany(mappedBy = "corretor")
	//@JoinColumn(name = "ID_SERVICOS_CONTRATADOS")
	private List<ServicoContratado> servicosContratados = new ArrayList<ServicoContratado>();


	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getComissao() {
		return comissao;
	}

	public void setComissao(double comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}
	
}
