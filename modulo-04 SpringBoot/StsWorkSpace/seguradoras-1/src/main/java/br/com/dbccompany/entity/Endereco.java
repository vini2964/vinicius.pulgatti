package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(allocationSize = 1, name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
@Inheritance(strategy = InheritanceType.JOINED)
public class Endereco {
	
	@Id
	@GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "LOGRADOURO", unique = true )
	private String logradouro;
	
	@Column(name = "NUMERO", unique = true )
	private long numero;
	
	@Column(name = "COMPLEMENTO")
	private String complemento;
	
	@ManyToOne
    @JoinColumn(name = "id_bairro")
	private Bairro bairro;
	
	@ManyToMany(mappedBy = "endereco")
	private List<Pessoas> pessoa = new ArrayList<>();
	
	@OneToOne(mappedBy = "seguradora")
	private EnderecoSeguradora enderecoSeguradora;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<Pessoas> getPessoa() {
		return pessoa;
	}

	public void setPessoa(List<Pessoas> pessoa) {
		this.pessoa = pessoa;
	}

	public EnderecoSeguradora getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}

	
	
	
}
