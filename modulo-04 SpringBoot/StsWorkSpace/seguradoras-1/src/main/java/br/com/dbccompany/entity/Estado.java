package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@SequenceGenerator(allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Estado {
	
	@Id
	@GeneratedValue(generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME",nullable = false, unique = true )
	private String nome;
	
	@OneToMany(mappedBy = "estado")
    private List<Cidade> cidade = new ArrayList<>();
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cidade> getCidade() {
		return cidade;
	}

	public void setCidade(List<Cidade> cidade) {
		this.cidade = cidade;
	}	
	
}
