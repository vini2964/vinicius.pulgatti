package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoas {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "PESSOA_SEQ", sequenceName = "PESSOA_SEQ")
	@GeneratedValue(generator = "PESSOA_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME",nullable = false, unique = true )
	private String nome;
	
	@Column(name = "CPF",nullable = false, unique = true )
	private long cpf;
	
	@Column(name = "PAI",nullable = false, unique = true )
	private String pai;
	
	@Column(name = "MAE",nullable = false, unique = true )
	private String mae;
	
	@Column(name = "TELEFONE",nullable = false, unique = true )
	private long telefone;
	
	@Column(name = "EMAIL",nullable = false, unique = true )
	private String email;
	
	@ManyToMany
	 @JoinTable(name = "ENDERECO_PESSOAS",
	            joinColumns = {
	                @JoinColumn(name = "ID_PESSOAS")},
	            inverseJoinColumns = {
	                @JoinColumn(name = "ID_ENDERECOS")})
	private List<Endereco> endereco = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Endereco> getEndereco() {
		return endereco;
	}

	public void setEndereco(List<Endereco> endereco) {
		this.endereco = endereco;
	}
	
	
	
	
}
