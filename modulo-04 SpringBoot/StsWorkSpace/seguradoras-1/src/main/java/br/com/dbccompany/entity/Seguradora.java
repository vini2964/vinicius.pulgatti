package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Seguradora {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SEGURADORA_SEQ", sequenceName = "SEGURADORA_SEQ")
	@GeneratedValue(generator = "SEGURADORA_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME",nullable = false, unique = true )
	private String nome;
	
	@Column(name = "CNPJ",nullable = false, unique = true )
	private long cnpj;
	
	@OneToOne(mappedBy = "seguradora")
	@JoinColumn(name = "ID_ENDERECO_SEGURADORA")
	private EnderecoSeguradora enderecoSeguradora;
	
	@OneToMany(mappedBy = "seguradora")
    private List<ServicoContratado> servicoContratado = new ArrayList<>();
	
	@ManyToMany
	 @JoinTable(name = "Seguradora_Servicos",
	            joinColumns = {
	                @JoinColumn(name = "ID_SEGURADORA")},
	            inverseJoinColumns = {
	                @JoinColumn(name = "ID_SERVICOS")})
	private List<Servicos> servicos = new ArrayList<>();

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}


	public EnderecoSeguradora getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}

	
	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servicos> servicos) {
		this.servicos = servicos;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
	
}
