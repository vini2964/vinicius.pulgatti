package br.com.dbccompany.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "SERVICO_CONTRATADO_SEQ", sequenceName = "SERVICO_CONTRATADO_SEQ")

public class ServicoContratado {
	
	@Id
	@GeneratedValue(generator = "SERVICO_CONTRATADO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "DESCRICAO",nullable = false, unique = true )
	private String descricao;
	
	@Column(name = "VALOR",nullable = false, unique = true )
	private long valor;
	
	@ManyToOne
    @JoinColumn(name = "ID_SEGURADORA")
	private Seguradora seguradora;
	
	@ManyToOne
    @JoinColumn(name = "ID_SERVICOS")
	private Servicos servico;
	
	@ManyToOne
	@JoinColumn(name = "ID_PESSOAS_SEGURADOS" )
	private Segurados segurados;
	
	@ManyToOne
	@JoinColumn(name = "ID_PESSOAS_CORRETOR")
	private Corretor corretor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public Servicos getServico() {
		return servico;
	}

	public void setServico(Servicos servico) {
		this.servico = servico;
	}

	public Segurados getSegurados() {
		return segurados;
	}

	public void setSegurados(Segurados segurados) {
		this.segurados = segurados;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}
	
}
