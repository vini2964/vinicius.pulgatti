package br.com.dbccompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "SERVICOS_SEQ", sequenceName = "SERVICOS_SEQ")

public class Servicos {
	
	@Id
	@GeneratedValue(generator = "SERVICOS_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME",unique = true )
	private String nome;
	
	@Column(name = "DESCRICAO", unique = true )
	private String descricao;
	
	
	@Column(name="VALOR_PADRAO" )
	private long valorPadrao;

	
	@OneToMany(mappedBy = "servico")
    private List<ServicoContratado> servicoContratado = new ArrayList<>();
	
	public List<Seguradora> getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(List<Seguradora> seguradora) {
		this.seguradora = seguradora;
	}

	@ManyToMany(mappedBy = "servicos")
	private List<Seguradora> seguradora = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(long valorPadrao) {
		this.valorPadrao = valorPadrao;
	}


	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
	
}
