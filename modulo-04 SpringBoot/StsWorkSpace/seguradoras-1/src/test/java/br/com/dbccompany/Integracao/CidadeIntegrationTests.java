package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Repository.CidadeRepository;
import br.com.dbccompany.Repository.EstadoRepository;
import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CidadeIntegrationTests {
	
	@MockBean
	private CidadeRepository cidadeRepository;
	
	@MockBean
	private EstadoRepository estadoRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Estado estado = new Estado();
		estado.setNome("Parana");
		
		Cidade cidade = new Cidade();
		cidade.setNome("Porto Alegre");
		cidade.setEstado(estado);
		
		
		Mockito.when(cidadeRepository.findByNome(cidade.getNome())).thenReturn(cidade);
		Mockito.when(cidadeRepository.findByestado(estado)).thenReturn(cidade);
		Mockito.when(estadoRepository.findByNome(estado.getNome())).thenReturn(estado);
	}

	@Test
	public void AchaCidadePorNome() {
		String nome = "Porto Alegre";
		
		Cidade found = cidadeRepository.findByNome(nome);
		
		assertThat(found.getNome()).isEqualTo(nome);
	}
	
	@Test
	public void AchaCidadePorEstado() {
		String nome = "Parana";
		String nomeCidade = "Porto Alegre";
				
		Estado estado = estadoRepository.findByNome(nome);
		Cidade found = cidadeRepository.findByestado(estado);
		
		assertThat(found.getNome()).isEqualTo(nomeCidade);	
	}
	
}
