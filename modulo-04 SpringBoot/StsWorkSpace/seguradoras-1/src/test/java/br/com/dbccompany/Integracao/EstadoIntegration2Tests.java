package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Repository.EstadoRepository;
import br.com.dbccompany.entity.Estado;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegration2Tests {

	@MockBean
	private EstadoRepository estadoRepository;
			
	@Autowired
	ApplicationContext context;
			
	@Before
	public void setUp() {
		Estado estado = new Estado();
		estado.setNome("Rio Grande Do Sul");
				
		Mockito.when(estadoRepository.findByNome(estado.getNome())).thenReturn(estado);
	} 
			
	@Test
	public void AchaEstadoPorNomeMockito() {
		String nome = "Rio Grande Do Sul";
		Estado found = estadoRepository.findByNome(nome);
				
		assertThat(found.getNome()).isEqualTo(nome);
				
				
	}
}
