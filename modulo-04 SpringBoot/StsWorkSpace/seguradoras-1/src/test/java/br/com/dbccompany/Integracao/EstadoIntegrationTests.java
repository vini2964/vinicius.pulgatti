package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Repository.EstadoRepository;
import br.com.dbccompany.entity.Cidade;
import br.com.dbccompany.entity.Estado;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegrationTests {

		@Autowired
		private TestEntityManager entityManager;
		
		@Autowired
		private EstadoRepository estadoRepository;

		@Test
		public void AchaEstadoPorNome() {
			Estado estado = new Estado();
			estado.setNome("Rio Grande do Sul");
			entityManager.persist(estado);
			entityManager.flush();
			
			Estado found = estadoRepository.findByNome(estado.getNome());
			
			assertThat(found.getNome()).isEqualTo(estado.getNome());
		}
		
		@Test
		public void AchaListaCidadeEmEstado() {
			Estado estado = new Estado();
			estado.setNome("Rio Grande do Sul");
			
			
			List<Cidade> cidades = new ArrayList();
			Cidade cidade = new Cidade();
			cidade.setNome("porto Alegre");
			cidade.setEstado(estado);
			cidades.add(cidade);
			
			estado.setCidade(cidades);
			entityManager.persist(estado);
			entityManager.persist(cidade);
			entityManager.flush();
			
			Estado foundEstado = estadoRepository.findByCidade(cidades);
			
			assertThat(foundEstado.getCidade().get(0).getNome()).isEqualTo(cidade.getNome());
			assertThat(foundEstado.getCidade().size()).isEqualTo(1);
		}
		
		@Test
		public void Acha5CidadesEmEstado() {
			Estado estado = new Estado();
			estado.setNome("Rio Grande do Sul");
			
			
			List<Cidade> cidades = new ArrayList();
			
			Cidade cidade1 = new Cidade();
			cidade1.setNome("porto Alegre");
			cidade1.setEstado(estado);
			
			Cidade cidade2 = new Cidade();
			cidade2.setNome("alvorada");
			cidade2.setEstado(estado);
			
			Cidade cidade3 = new Cidade();
			cidade3.setNome("canoas");
			cidade3.setEstado(estado);
			
			Cidade cidade4 = new Cidade();
			cidade4.setNome("gravatai");
			cidade4.setEstado(estado);
			
			Cidade cidade5 = new Cidade();
			cidade5.setNome("arroio do sal");
			cidade5.setEstado(estado);
			
			cidades.add(cidade1);
			cidades.add(cidade2);
			cidades.add(cidade3);
			cidades.add(cidade4);
			cidades.add(cidade5);

			
			estado.setCidade(cidades);
			
			entityManager.persist(estado);
			entityManager.persist(cidade1);
			entityManager.persist(cidade2);
			entityManager.persist(cidade3);
			entityManager.persist(cidade4);
			entityManager.persist(cidade5);
			entityManager.flush();
			
			Estado foundEstado = estadoRepository.findByCidade(cidades);
			
			assertThat(foundEstado.getCidade().get(3).getNome()).isEqualTo("gravatai");
			//assertThat(foundEstado.getCidade().size()).isEqualTo(5);
		}
		

}
