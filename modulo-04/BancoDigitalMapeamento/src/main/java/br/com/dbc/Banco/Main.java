package br.com.dbc.Banco;


import br.com.dbc.Banco.entity.Banco;
import br.com.dbc.Banco.entity.Cliente;
import br.com.dbc.Banco.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Banco banco = new Banco();
            banco.setNome("Santander");
            banco.setCodigo(4);
            session.save(banco); 
            
            Banco banco2 = new Banco();
            banco2.setNome("Itau");
            banco2.setCodigo(2);
            session.save(banco2);
           
            Cliente clienteSantander = new Cliente();
            clienteSantander.setNome("Antonio");
            clienteSantander.setCpf(9);
            clienteSantander.setRg(123);
            clienteSantander.setSaldo(100);
            session.save(clienteSantander);
            
            Cliente clienteSantander2 = new Cliente();
            clienteSantander2.setNome("Andre");
            clienteSantander2.setCpf(8184);
            clienteSantander2.setRg(1234);
            clienteSantander2.setSaldo(100);
            session.save(clienteSantander2);
            
            Cliente clienteItau1 = new Cliente();
            clienteItau1.setNome("ZÉ");
            clienteItau1.setCpf(147);
            clienteItau1.setRg(1);
            clienteItau1.setSaldo(100);
            session.save(clienteItau1);
            
            Cliente clienteItau2 = new Cliente();
            clienteItau2.setNome("maria");
            clienteItau2.setCpf(146);
            clienteItau2.setRg(13);
            clienteItau2.setSaldo(100);
            session.save(clienteItau2);
            
            clienteSantander.setSaldo(0);
            clienteSantander.setEmprestimo(84013);
            session.save(clienteSantander);
            clienteSantander2.setSaldo(0);
            clienteSantander2.setEmprestimo(849165);
            session.save(clienteSantander2);
            clienteItau1.setSaldo(0);
            clienteItau1.setEmprestimo(8461);
            session.save(clienteItau1);
            clienteItau2.setSaldo(400);
            session.save(clienteItau2);
            
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }
        } finally {
            if (session != null) {
                session.close();
                System.exit(0);
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
}
