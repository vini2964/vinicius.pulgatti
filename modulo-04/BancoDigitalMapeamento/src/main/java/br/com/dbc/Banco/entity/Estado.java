/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Banco.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author vinicius.pulgatti
 */
@Entity
@Table(name="estado")
public class Estado {
     @Id
    @SequenceGenerator(allocationSize = 1,name = "estado_seq", sequenceName = "estado_seq")
    @GeneratedValue(generator = "estado_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
     
    @Column (name = "nome") 
    private String nome;
    
    @Column (name = "id_pais")
    private Integer id_pais;
    
    @Column (name = "sigla")
    private String sigla;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId_pais() {
        return id_pais;
    }

    public void setId_pais(Integer id_pais) {
        this.id_pais = id_pais;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    

  
    
}
