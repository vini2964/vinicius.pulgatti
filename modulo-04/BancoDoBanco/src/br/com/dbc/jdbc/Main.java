/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vinicius.pulgatti
 */
public class Main {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) {
       Connection conn = Connector.connect();
       try {
           ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'CLIENTE'")
                   .executeQuery();
            
                PreparedStatement pst = conn.prepareStatement("update CLIENTE set DINHEIRO =? WHERE ID =?"
                ); 
            pst.setInt(1,100);
            pst.setInt(2, 2);
            
            pst.executeUpdate();
            
            rs = conn.prepareStatement("select * from CLIENTE").executeQuery();
               
             /*   PreparedStatement pst = conn.prepareStatement("alter table CLIENTE add  dinheiro number"); 
            pst.executeUpdate();
            
            rs = conn.prepareStatement("select * from CLIENTE").executeQuery(); */
            
            
            } catch (SQLException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta do Main", ex);
       }
   }
   
}
  
