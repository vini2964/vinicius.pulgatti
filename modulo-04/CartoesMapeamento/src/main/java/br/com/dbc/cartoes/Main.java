package br.com.dbc.cartoes;

import br.com.dbc.cartoes.entity.Bandeira;
import br.com.dbc.cartoes.entity.Cartao;
import br.com.dbc.cartoes.entity.Cliente;
import br.com.dbc.cartoes.entity.Credenciador;
import br.com.dbc.cartoes.entity.Emissor;
import br.com.dbc.cartoes.entity.HibernateUtil;
import br.com.dbc.cartoes.entity.Lancamento;
import br.com.dbc.cartoes.entity.Loja;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Loja loja = new Loja();
            loja.setNome("Itau");
            session.save(loja);
           
            Cliente cliente = new Cliente();
            cliente.setNome("Vinicius");
            session.save(cliente);
           
            Credenciador credenciador = new Credenciador();
            credenciador.setNome("abcd");
            session.save(credenciador); 
            
            
            Bandeira bandeira = new Bandeira();
            bandeira.setNome("Master Card");
            bandeira.setTaxa(10);
            session.save(bandeira);
            
            Emissor emissor = new Emissor();
            emissor.setNome("Santander");
            emissor.setTaxa(10);
            session.save(emissor); 
           
            Cartao cartao = new Cartao();
            cartao.setChip ("Gold");
            cartao.setId_cliente(1);
            cartao.setId_bandeira(1);
            cartao.setId_emissor(1);
            cartao.setVencimento("10/04");
            session.save(cartao);
            
            Lancamento lancamento = new Lancamento();
            lancamento.setCartao(cartao);
            lancamento.setData_compra(10082018);
            lancamento.setDescricao("venda camiseta");
            lancamento.setEmissor(emissor);
            lancamento.setLoja(loja);
            lancamento.setValor(150);
            session.save(lancamento);
            
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }
        } finally {
            if (session != null) {
                session.close();
                System.exit(0);
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
}
