/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.cartoes.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author vinicius.pulgatti
 */
@Entity
@Table(name="cartao")
public class Cartao {
     @Id
    @SequenceGenerator(allocationSize = 1,name = "cartao_seq", sequenceName = "cartao_seq")
    @GeneratedValue(generator = "cartao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
   
     @OneToOne(mappedBy = "cartao") 
        private Lancamento lancamento;
    
    @Column (name = "chip")
    private String chip;

   @Column (name="id_cliente")
   private Integer id_cliente;
   
   @Column (name="id_bandeira")
   private Integer id_bandeira;
   
   @Column(name="id_emissor")
   private Integer id_emissor;
   
   @Column (name = "vencimento")
   private String vencimento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getChip() {
        return chip;
    }

    public void setChip(String taxa) {
        this.chip = chip;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_bandeira() {
        return id_bandeira;
    }

    public void setId_bandeira(Integer id_bandeira) {
        this.id_bandeira = id_bandeira;
    }

    public Integer getId_emissor() {
        return id_emissor;
    }

    public void setId_emissor(Integer id_emissor) {
        this.id_emissor = id_emissor;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

   
    
    
   
    
}
