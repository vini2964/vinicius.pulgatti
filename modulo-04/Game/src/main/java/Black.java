
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vinicius.pulgatti
 */
@DiscriminatorValue ("black")
public class Black extends CartaoTabelao {
   
    
    @Column(name = "black_pontos")
    private Double blackPontos;
    
} 

