/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos;

import br.com.dbc.jogos.dto.ClienteDTO;
import br.com.dbc.jogos.dto.ContaDTO;
import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.service.ClienteService;
import br.com.dbc.jogos.service.EnderecoService;

/**
 *
 * @author tiago
 */
       
public class Main {
    public static void main(String[] args) {
        EnderecoService instanceEndereco = new EnderecoService();
        ClienteDTO dto = new ClienteDTO();
        dto.setNomeCliente("Cliente 1");
        dto.setCpfCliente("123");
        EnderecoDTO eDTO = new EnderecoDTO();
        eDTO.setLogradouroEndereco("Rua 1");
        eDTO.setNumeroEndereco(1);
        eDTO.setBairroEndereco("Bairro 1");
        eDTO.setCidadeEndereco("Cidade 1");
        dto.setEnderecoDTO(eDTO);
        instanceEndereco.deletarEndereco(0);
        ClienteService instance = new ClienteService();
        instance.salvarCliente(dto);
        ContaDTO dtoConta = new ContaDTO();
        dtoConta.setNumero("2323");
        dtoConta.setAgencia("sdfsd");
        dtoConta.setBanco("sdfasfsa");
        dtoConta.setCliente(dto);
       
        System.exit(0);
    }
   
}
