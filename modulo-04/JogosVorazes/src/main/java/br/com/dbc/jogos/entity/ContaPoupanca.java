/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 *
 * @author tiago
 */
@Entity
@PrimaryKeyJoinColumn(name = "id_conta")
public class ContaPoupanca extends Conta {

    @Column(name = "deposito_mensal", nullable = false, precision = 10, scale = 2)
    private Double depositoMensal;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ContaType tipo = ContaType.POUPANCA;

    public Double getDepositoMensal() {
        return depositoMensal;
    }

    public void setDepositoMensal(Double depositoMensal) {
        this.depositoMensal = depositoMensal;
    }

}
