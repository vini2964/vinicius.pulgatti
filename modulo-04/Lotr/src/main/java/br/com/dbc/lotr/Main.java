package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.entity.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("Jose");
        dto.setNomeUsuario("Joao");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("5161");
        
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("rua do joao");
        enderecoDTO.setNumero(5);
        enderecoDTO.setBairro("jardim joao");
        enderecoDTO.setCidade("AVORADA");
        enderecoDTO.setComplemento("casa");
        dto.setEndereco(enderecoDTO);
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("elfjoao");
        personagemDTO.setDanoElfo(132d);
        dto.setPersonagem(personagemDTO);
        
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
    }
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();

             Usuario usuario = new Usuario();
            usuario.setNome("Antonio");
            usuario.setCpf(123l);
            usuario.setSenha("456");
            usuario.setApelido("TONINHO DA CACHOEIRA");
            
            Endereco endereco = new Endereco();
            endereco.setLogradouro("rua da penha");
            endereco.setBairro("j.algarve");
            endereco.setCidade("alvorada");
            endereco.setComplemento("");
            endereco.setNumero(13);
            
            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("rua da penha");
            endereco2.setBairro("j.algarve");
            endereco2.setCidade("alvorada");
            endereco2.setComplemento("casa");
            endereco2.setNumero(14);
            
            TipoContato tipoContato = new TipoContato();
            tipoContato.setNome("whats");
            tipoContato.setQuantidade(1);
            
            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("51982156616");
            
            tipoContato.setContato(contato);
            usuario.pushContato(contato);
            usuario.pushEnderecos(endereco,endereco2);
            
            
            session.save(usuario); 
            
            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%algarve"),
                            Restrictions.ilike("endereco.cidade", "%alvorada")
                    ));

            List<Usuario> usuarios = criteria.list();

            System.out.println(usuarios);
            usuarios.forEach(System.out::println);
            
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%algarve"),
                            Restrictions.ilike("endereco.cidade", "%alvorada")
                    ));
            
            //somar enderecos com criteria
            criteria.setProjection(Projections.sum("endereco.numero"));
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%algarve"),
                            Restrictions.ilike("endereco.cidade", "%alvorada")
                    ));
            System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));

            criteria.setProjection(Projections.count("id"));
            System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            
            usuarios = session.createQuery("select u from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%alvorada' "
                    + " and lower(endereco.bairro) like '%algarve' ")
                    .list();
            
            usuarios.forEach(System.out::println);
            
            //contagem de usuario com HQL
            Long count = (Long)session
                    .createQuery("select count(distinct u.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%alvorada' "
                    + " and lower(endereco.bairro) like '%algarve' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s usuarios", count));
           
            //contagem de endereco com HQL
            count = (Long)session
                    .createQuery("select count(endereco.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%alvorada' "
                    + " and lower(endereco.bairro) like '%algarve' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s valores", count));
            
            //Soma numeros dos enderecos
            Long sum = (Long)session
                    .createQuery("select sum(endereco.numero) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%alvorada' "
                    + " and lower(endereco.bairro) like '%algarve' ")
                    .uniqueResult();
            System.out.println(String.format("Somamos com HQL %s valores", count));
            
            
            
            //Conta quantidade de Linhas
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%algarve"),
                            Restrictions.ilike("endereco.cidade", "%alvorada")
                    ));
            criteria.setProjection(Projections.rowCount());
             System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            //conta contagem de enderecos
             criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%algarve"),
                            Restrictions.ilike("endereco.cidade", "%alvorada")
                    ));
             criteria.setProjection(Projections.count("enderecos"));
             System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
             
             
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("VINICIUS");
            session.save(elfoTabelao);

            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(60d);
            hobbitTabelao.setNome("hobbit");
            session.save(hobbitTabelao); 

            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("legolas");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);
            
            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("anao");
            hobbitPerClass.setDanoHobbit(10d);
            session.save(hobbitPerClass);
            
            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("legolas");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);
            
            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("anao");
            hobbitJoin.setDanoHobbit(10d);
            session.save(hobbitJoin);
            
            
            
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                System.exit(1);
            }
        } finally {
            if (session != null) {
                session.close();
                
            }
        }
        System.exit(0);
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
}
