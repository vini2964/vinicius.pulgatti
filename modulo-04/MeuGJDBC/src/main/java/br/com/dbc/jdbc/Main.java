/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vinicius.pulgatti
 */
public class Main {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) {
       Connection conn = Connector.connect();
       try {
           
           conn.prepareStatement("CREATE TABLE LOJA (\n"
                   + "  ID NUMBER NOT NULL PRIMARY KEY,\n"
                   + "  NOME VARCHAR(100) NOT NULL,\n"
                   + ")").execute();
            
            } catch (SQLException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta do Main", ex);
       }
   }
   
}
  
